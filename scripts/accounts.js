const fs = require('fs')
const path = require('path')
require('colors')

const { MerkleTree } = require('merkletreejs');
const keccak256 = require('keccak256');
const { ethers, network } = require('hardhat');
const assert = require('assert');

async function main() {

    console.log()
    console.log('                                                                                       '.bgGrey.white.bold)
    console.log('       FunRise accounts                                                                '.bgGrey.white.bold)
    console.log('                                                                                       '.bgGrey.white.bold)
    console.log()

    let params = {
        'FR_NETWORK': null,
        'FR_OWNER_PRIVATE_KEY': null,
        'FR_MARKETPLACE_ADDRESS': null,
        'FR_RECIPIENT_ADDRESS': null,
        'FR_ACCOUNTS_NUM': null,
        'FR_VALUE': null,
        'FR_PROVIDER_RPC': null,
        'FR_ACTION': null
    }
    Object.entries(params).map(([name, val]) => {
        params[name] = process.env[name] || ''
        console.log(name, params[name].bgBlack.brightYellow.bold)
    })
    Object.entries(params).map(([name, val]) => {
        if (name != 'FR_ACTION' || name != 'FR_PREVIEW') assert(!!params[name], name + ' is not defined')
    })
    console.log()
    assert(network.name == params['FR_NETWORK'], 'netowrk mismatch')

    const action = params['FR_ACTION']

    let provider = new ethers.providers.JsonRpcProvider(params['FR_PROVIDER_RPC'])

    process.env.FR_PREVIEW = process.env.FR_PREVIEW.replace('0', '')

    let owner = new ethers.Wallet(params['FR_OWNER_PRIVATE_KEY'], provider)
    console.log('owner address', owner.address.toString().bgBlack.brightYellow.bold)
    console.log('owner balance', ethers.utils.formatEther(await owner.getBalance()).bgBlack.brightGreen.bold)
    console.log()

    let impersonated_address = '0xc79dF9fe252Ac55AF8aECc3D93D20b6A4A84527B'

    let accounts_num = parseInt(params['FR_ACCOUNTS_NUM'])

    if (action) {
        console.log()
    } else {
        return
    }

    let accounts_path = path.join(__dirname, 'data', `accounts.${network.name}.json`)
    let accounts_tree_path = path.join(__dirname, 'data', `accounts.tree.${network.name}.json`)

    const artifacts_dir = path.resolve('artifacts/contracts')
    const erc20_abi = require(path.join(artifacts_dir, 'tokens', 'EERC20Burnable.sol', 'EERC20Burnable.json')).abi

    let erc20_address = '0x2791bca1f2de4661ed88a30c99a7a9449aa84174'
    let erc20_decimals = '6'

    let usdc = new ethers.Contract(
        erc20_address,
        erc20_abi,
        provider
    )

    let hash_address = (address) => {
        return Buffer.from(ethers.utils.solidityKeccak256(['address'], [address]).slice(2), 'hex')
    }

    let load_accounts = async () => {

        return require(accounts_path)
    }

    let load_accounts_tree = async () => {

        return require(accounts_tree_path)
    }

    let generate = async () => {

        console.log('GENERATE'.bgBlack.brightYellow.bold)

        let accounts = {}
        for (let i = 0; i < accounts_num; i += 1) {
            let wallet = ethers.Wallet.createRandom();
            accounts[wallet.address] = {
                private_key: wallet.privateKey,
                phrase: wallet.mnemonic.phrase
            }
        }

        fs.writeFileSync(accounts_path, JSON.stringify(accounts, null, 2), 'utf-8')

        console.log('saved to', accounts_path.bgBlack.brightYellow.bold)
        console.log()
    }

    let refill = async () => {

        console.log('REFILL'.bgBlack.brightYellow.bold)
        console.log()

        let accounts = await load_accounts()
        assert(accounts_num == Object.keys(accounts).length, 'accounts number mismatch')

        for (let address in accounts) {

            if (!!process.env.FR_PREVIEW) {
                console.log('preview')
                continue
            }

            let tx = {
                nonce: await provider.getTransactionCount(owner.address, 'pending'),
                chainId: (await provider.getNetwork()).chainId,
                gasLimit: ethers.BigNumber.from(2e5),
                gasPrice: await provider.getGasPrice(),
                to: address,
                value: ethers.utils.parseEther(params['FR_VALUE'])
            }
            tx = await owner.sendTransaction(tx)
            tx.wait()

            if (impersonated_address && network.name.includes('_fork')) {

                let erc20_amount = '1000'

                await network.provider.request({
                    method: "hardhat_impersonateAccount",
                    params: [impersonated_address],
                });
                const impersonated = await ethers.getSigner(impersonated_address)
                usdc = usdc.connect(impersonated)
                await (await usdc.transfer(
                    address,
                    ethers.utils.parseUnits(erc20_amount, erc20_decimals)
                )).wait()

                console.log(
                    address.bgBlack.brightYellow.bold,
                    'erc20 balance',
                    ethers.utils.formatUnits(
                        await usdc.balanceOf(address),
                        erc20_decimals
                    ).bgBlack.brightGreen.bold
                )
            } else {
                let erc20_amount = '0.02'
                let usdc = new ethers.Contract(
                    erc20_address,
                    erc20_abi,
                    owner
                )
                await (await usdc.transfer(
                    address,
                    ethers.utils.parseUnits(erc20_amount, erc20_decimals)
                )).wait()
            }

            console.log(address.bgBlack.brightYellow.bold, 'after balance', ethers.utils.formatEther(await provider.getBalance(address)).bgBlack.brightGreen.bold)
        }

        console.log()
        console.log('owner balance', ethers.utils.formatEther(await owner.getBalance()).bgBlack.brightGreen.bold)
    }

    let withdraw = async () => {

        let accounts = await load_accounts()

        if (params['FR_RECIPIENT_ADDRESS'] == "NONE") {
            console.log('no recipient')
            return
        }
        
        console.log('WITHDRAW'.bgBlack.brightYellow.bold, params['FR_VALUE'])
        console.log('to FR_RECIPIENT_ADDRESS', params['FR_RECIPIENT_ADDRESS'].bgBlack.brightYellow.bold)
        console.log()

        console.log(params['FR_RECIPIENT_ADDRESS'].bgBlack.brightYellow.bold, 'balance', ethers.utils.formatEther(await provider.getBalance(params['FR_RECIPIENT_ADDRESS'])).bgBlack.brightGreen.bold)
        console.log()

        assert(accounts_num == Object.keys(accounts).length, 'accounts number mismatch')

        let gasPrice = await provider.getGasPrice()

        for (let address in accounts) {

            let wallet = new ethers.Wallet(accounts[address], provider)
            console.log(address.bgBlack.brightYellow.bold, 'before balance', ethers.utils.formatEther(await provider.getBalance(address)).bgBlack.brightGreen.bold)

            if (!process.env.FR_PREVIEW) {
            
                let tx = {
                    nonce: await provider.getTransactionCount(wallet.address, 'pending'),
                    chainId: (await provider.getNetwork()).chainId,
                    gasPrice: gasPrice,
                    to: params['FR_RECIPIENT_ADDRESS'],
                    value: ethers.utils.parseEther(params['FR_VALUE'])
                }
                let gas = await provider.estimateGas(tx)
                tx.value = tx.value.sub(gasPrice.mul(gas))
                
                tx = await wallet.sendTransaction(tx)
                tx.wait()
            } else {
                console.log('preview')
            }
        }

        console.log()
        console.log(params['FR_RECIPIENT_ADDRESS'].bgBlack.brightYellow.bold, 'after balance', ethers.utils.formatEther(await provider.getBalance(params['FR_RECIPIENT_ADDRESS'])).bgBlack.brightGreen.bold)
    }

    let register = async () => {

        console.log('REGISTER'.bgBlack.brightYellow.bold)

        let accounts = await load_accounts()
        assert(accounts_num == Object.keys(accounts).length, 'accounts number mismatch')

        let leafs = []
        for (let address in accounts) {
            // console.log(address.bgBlack.brightYellow.bold, 'balance', ethers.utils.formatEther(await provider.getBalance(address)))
            leafs.push(hash_address(address))
        }

        let merkle_tree = new MerkleTree(leafs, keccak256, { sortPairs: true });
        let root = merkle_tree.getHexRoot()

        Object.entries(accounts).map(([address, val]) => {
            accounts[address] = {
                private_key: val.private_key,
                phrase: val.phrase,
                proof: merkle_tree.getHexProof(hash_address(address)),
                root: root
            }
        })

        fs.writeFileSync(accounts_tree_path, JSON.stringify(accounts, null, 2), 'utf-8')

        let gas_facet = (await ethers.getContractFactory('GasFacet')).attach(params['FR_MARKETPLACE_ADDRESS']).connect(owner)

        console.log('root', root)
        console.log()

        await gas_facet.setRoot(root, { gasLimit: ethers.BigNumber.from(1e6) })
    }

    let transfer_all = async () => {
        let wallet = ethers.Wallet.createRandom();
        console.log(wallet.address)
        console.log(wallet.privateKey)
        console.log(wallet.mnemonic.phrase)

        let gasPrice = await provider.getGasPrice()

        if (!process.env.FR_PREVIEW) {
            let tx = {
                nonce: await provider.getTransactionCount(owner.address),
                chainId: (await provider.getNetwork()).chainId,
                gasLimit: ethers.BigNumber.from(2e5),
                gasPrice: gasPrice,
                to: wallet.address,
                value: await provider.getBalance(owner.address)
            }
            tx.value = tx.value.sub(ethers.utils.parseEther('0.01'))
            console.log('transfer')
            tx = await owner.sendTransaction(tx)
            tx.wait()
        } else {
            console.log('preview')
        }

        console.log(owner.address.bgBlack.brightYellow.bold, 'owner balance', ethers.utils.formatEther(await provider.getBalance(owner.address)).bgBlack.brightGreen.bold)
        console.log(wallet.address.bgBlack.brightYellow.bold, 'new account balance', ethers.utils.formatEther(await provider.getBalance(wallet.address)).bgBlack.brightGreen.bold)
    }

    action == 'GENERATE' && await generate()
    action == 'REFILL' && await refill()
    action == 'WITHDRAW' && await withdraw()
    action == 'REGISTER' && await register()

    action == 'TRANSFER_ALL' && await transfer_all()
}

main()
    .then(() => process.exit(0))
    .catch(error => {
        console.error(error)
        process.exit(1)
    })
