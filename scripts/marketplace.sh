 #!/usr/bin/bash 

source $(dirname $0)/check.sh

export FR_PREVIEW=$1
export FR_NETWORK=$2
export FR_ACTION=$3
export FR_VALUE=$4

echo "args FR_PREVIEW FR_NETWORK FR_ACTION FR_VALUE"
echo ""

if [ $1 == "show" ]
then
exit
fi

if [ $FR_NETWORK == "matic_fork" ]
then
export FR_PROVIDER_RPC="http://0.0.0.0:8545"
elif [ $FR_NETWORK == "matic" ]
then
export FR_PROVIDER_RPC="https://polygon-mainnet.infura.io/v3/${FR_INFURA_ID}"
else
echo "bad network"
exit
fi
echo "set FR_PROVIDER_RPC to $FR_PROVIDER_RPC"

export FR_VALUE="${FR_VALUE:-NONE}"

echo "FR_MARKETPLACE_ADDRESS $FR_MARKETPLACE_ADDRESS"
echo "FR_VALUE $FR_VALUE"
echo "FR_PROVIDER_RPC $FR_PROVIDER_RPC"
echo "FR_ACTION $FR_ACTION"
echo ""

if [ $1 == "1" ]
then
export FR_PREVIEW="1"
fi

sleep 1

npx hardhat run scripts/marketplace.js --network $FR_NETWORK


