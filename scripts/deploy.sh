 #!/usr/bin/bash 

source $(dirname $0)/check.sh

echo "args: FR_NETWORK"
echo ""

if [ -z "$FR_PROVIDER_RPC" ] 
then
	echo "FR_PROVIDER_RPC is not defined"
    exit 0
fi

export FR_NETWORK=$1

if [ $1 == "show" ]
then
exit
fi

sleep 1

npx hardhat run scripts/deploy.js --network $FR_NETWORK


