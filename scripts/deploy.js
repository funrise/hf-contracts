const fs = require('fs')
const path = require('path')
require('colors')

const { ethers, network } = require('hardhat')
const { getSelectors, FacetCutAction } = require('./libraries/diamond.js')

const network_name = network.name

process.env.NODE_CONFIG_ENV = network_name
process.env.NODE_ENV = "production"
var config = require('config')

async function main() {
    console.log()
    console.log('                                                                                       '.bgGrey.white.bold)
    console.log('       FunRise deployment                                                              '.bgGrey.white.bold)
    console.log('                                                                                       '.bgGrey.white.bold)
    console.log()

    // ---------------------------------------------------------------------------------------------------
    // ---------------- Config ---------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------

    console.log('config network', config.network.bgBlack.grey.bold)
    console.log('provider network', network_name.bgBlack.grey.bold)
    if (config.network != network_name) throw new Error('provider network / config mismatch')

    const out_config_dir = path.resolve('config/deployed')
    const artifacts_dir = path.resolve('artifacts/contracts')
    const subgraph_artifacts_dir = path.resolve('subgraph/funrise/abis')

    let rpc_url = config.provider_rpc
    console.log('rpc url', rpc_url.bgBlack.blue.bold)
    console.log()
    if (config.subgraph_endpoint) {
        console.log('subgraph endpoint', config.subgraph_endpoint.bgBlack.blue.bold)
        console.log()
    }

    let provider = new ethers.providers.JsonRpcProvider(rpc_url)
    let net = await provider.getNetwork()

    let ABI = {
        'Diamond': require(path.join(artifacts_dir, 'shared', 'Diamond.sol', 'Diamond.json')).abi,
        'DiamondCutFacet': require(path.join(artifacts_dir, 'shared', 'facets', 'DiamondCutFacet.sol', 'DiamondCutFacet.json')).abi,
        'DiamondLoupeFacet': require(path.join(artifacts_dir, 'shared', 'facets', 'DiamondLoupeFacet.sol', 'DiamondLoupeFacet.json')).abi,
        'OwnershipFacet': require(path.join(artifacts_dir, 'shared', 'facets', 'OwnershipFacet.sol', 'OwnershipFacet.json')).abi,

        'MarketplaceInit': require(path.join(artifacts_dir, 'marketplace', 'MarketplaceInit.sol', 'MarketplaceInit.json')).abi,
        'MarketBaseFacet': require(path.join(artifacts_dir, 'marketplace', 'facets', 'MarketBaseFacet.sol', 'MarketBaseFacet.json')).abi,
        'FixedPriceFacet': require(path.join(artifacts_dir, 'marketplace', 'facets', 'FixedPriceFacet.sol', 'FixedPriceFacet.json')).abi,
        'AuctionFacet': require(path.join(artifacts_dir, 'marketplace', 'facets', 'AuctionFacet.sol', 'AuctionFacet.json')).abi,
        'GasFacet': require(path.join(artifacts_dir, 'marketplace', 'facets', 'GasFacet.sol', 'GasFacet.json')).abi,

        'ENFT': require(path.join(artifacts_dir, 'tokens', 'ENFT.sol', 'ENFT.json')).abi,
        'EERC20Burnable': require(path.join(artifacts_dir, 'tokens', 'EERC20Burnable.sol', 'EERC20Burnable.json')).abi,
        'SwapRouter': require(path.join(artifacts_dir, 'router_v2_abi.js')).abi,
    }

    fs.writeFileSync(path.join(subgraph_artifacts_dir, 'ENFT.json'), JSON.stringify(ABI['ENFT'], null, 2), 'utf-8')
    fs.writeFileSync(path.join(subgraph_artifacts_dir, 'MarketBaseFacet.json'), JSON.stringify(ABI['MarketBaseFacet'], null, 2), 'utf-8')
    fs.writeFileSync(path.join(subgraph_artifacts_dir, 'FixedPriceFacet.json'), JSON.stringify(ABI['FixedPriceFacet'], null, 2), 'utf-8')
    fs.writeFileSync(path.join(subgraph_artifacts_dir, 'AuctionFacet.json'), JSON.stringify(ABI['AuctionFacet'], null, 2), 'utf-8')
    fs.writeFileSync(path.join(subgraph_artifacts_dir, 'GasFacet.json'), JSON.stringify(ABI['GasFacet'], null, 2), 'utf-8')

    console.log('network', `${network_name}`.bgBlack.brightYellow.bold, 'chainId', `${net.chainId}`.bgBlack.brightYellow.bold)
    console.log('block', `${await provider.getBlockNumber()}`.bgBlack.brightYellow.bold)
    console.log()

    var owner = new ethers.Wallet(process.env.FR_OWNER_PRIVATE_KEY, provider)

    config.ABI = ABI
    config.chain_id = net.chainId

    let gasPrice = await provider.getGasPrice()

    console.log('owner', owner.address.bgBlack.brightGreen.bold)
    console.log(
        'owner wrapped net token balance',
        ethers.utils.formatUnits(await (new ethers.Contract(
            config.wrapped_net_token.address,
            ABI['EERC20Burnable'],
            provider
        )).balanceOf(owner.address), config.wrapped_net_token.decimals).bgBlack.brightGreen.bold
    )
    console.log('owner balance', ethers.utils.formatUnits(await owner.getBalance(), '18').bgBlack.brightGreen.bold)
    console.log()

    // ---------------------------------------------------------------------------------------------------
    // ---------------- Contracts ------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------

    console.log(config.contract_addressess)
    console.log()

    let DeployContract = async (name, ...args) => {
        let Conteact = (await ethers.getContractFactory(name)).connect(owner)
        let contract = null
        if (!config.contract_addressess[name]) {
            console.log(name.bgBlack.brightYellow.bold, 'address is not defined. Deploying ...')
            contract = await (await Conteact.deploy(...args, {gasPrice: gasPrice.mul(2)} )).deployed()
            config.contract_addressess[name] = contract.address
            contract.is_new = true
        }
        console.log(name.bgBlack.brightYellow.bold, config.contract_addressess[name].bgBlack.brightGreen.bold)
        console.log()
        return contract || Conteact.attach(config.contract_addressess[name])
    }

    // ---------------- ERC20 --------------------------------------------------------------------------

    console.log('       ERC20                                                                           '.bgGrey.white.bold)
    console.log()

    for (let t of config.tokens) {

        if (!t.address) {
            console.log(t.name.bgBlack.brightYellow.bold, 'address is not defined. Deploying ...')

            let EERC20Burnable = (await ethers.getContractFactory('EERC20Burnable')).connect(owner)
            let supply = ethers.utils.parseUnits(t.supply, t.decimals)
            let token = await (await EERC20Burnable.deploy(t.name, t.symbol, supply, parseInt(t.decimals))).deployed()

            t.address = token.address
        }
        config.contract_addressess[t.symbol] = t.address

        if (t.is_default) {
            config.default_erc20_token = t
        }

        console.log(t.name.bgBlack.brightYellow.bold, t.address.bgBlack.brightGreen.bold)
        console.log()
    }

    // ---------------- Marketplace ----------------------------------------------------------------------

    console.log('       Marketplace                                                                     '.bgGrey.white.bold)
    console.log()

    let facets = {}
    facets.diamond_cut_facet = await DeployContract('DiamondCutFacet')
    facets.diamond_loupe_facet = await DeployContract('DiamondLoupeFacet')
    facets.ownership_facet = await DeployContract('OwnershipFacet')
    facets.marketplace_init = await DeployContract('MarketplaceInit')
    facets.marketbase_facet = await DeployContract('MarketBaseFacet')
    facets.fixedprice_facet = await DeployContract('FixedPriceFacet')
    facets.auction_facet = await DeployContract('AuctionFacet')
    facets.gas_facet = await DeployContract('GasFacet')
    let diamond = await DeployContract('Diamond', owner.address, facets.diamond_cut_facet.address)


    let MakeCut = async (facet_names, action, get_selectors=null) => {
        let Cut = async (diamond_address, init_address, function_call, cut) => {
            cut.forEach((c) => {
                console.log('Cut', c.name.bgBlack.brightYellow.bold, 'action', `${Object.keys(FacetCutAction)[c.action]}`.bgBlack.brightYellow.bold)
            })
            console.log('Cutting', `${diamond_address}`.bgBlack.brightGreen.bold, ' ...')
            const diamond_cut = (await ethers.getContractAt('IDiamondCut', diamond_address)).connect(owner)
            let tx = await diamond_cut.diamondCut(
                cut,
                action == FacetCutAction.Remove ? ethers.constants.AddressZero : init_address,
                action == FacetCutAction.Remove ? '0x' : function_call,
                { gasLimit: ethers.BigNumber.from(1e6), gasPrice: gasPrice.mul(2) }
            )
            let receipt = await tx.wait()
            if (!receipt.status) {
              throw Error(`Diamond upgrade failed: ${tx.hash}`.bgBlack.red.bold)
            }
            console.log('Cut', tx.hash.bgBlack.brightGreen.bold, 'completed')
            console.log()
        }
        let cut = []
        facet_names.forEach((facet_name) => {
            let selectors = getSelectors(facets[facet_name])
            if (get_selectors) {
                console.log(selectors)
                selectors = selectors.get(get_selectors)
                console.log(selectors)
            }
            cut.push({
                facetAddress: action == FacetCutAction.Remove ? ethers.constants.AddressZero : facets[facet_name].address,
                action: action,
                functionSelectors: selectors,
                name: facet_name
            })
        })
        console.log(facet_names, cut)
        await Cut(diamond.address, facets.marketplace_init.address, facets.marketplace_init.interface.encodeFunctionData('init'), cut)
    }

    // ---------------- DIAMOND CUTS START -------------------------------------------------------------

    await MakeCut(['diamond_loupe_facet', 'ownership_facet', 'marketplace_init'], FacetCutAction.Add)
    await MakeCut(['marketbase_facet', 'fixedprice_facet', 'auction_facet'], FacetCutAction.Add)
    await MakeCut(['gas_facet'], FacetCutAction.Add)

    // await MakeCut(['gas_facet'], FacetCutAction.Remove)
    
    // await MakeCut(['marketbase_facet'], FacetCutAction.Replace)
    // await MakeCut(['fixedprice_facet'], FacetCutAction.Replace)
    // await MakeCut(['auction_facet'], FacetCutAction.Replace)

    // ---------------- DIAMOND CUTS END ---------------------------------------------------------------

    await facets.marketbase_facet.attach(diamond.address).connect(owner).setExchangeToken(
        config.contract_addressess['USDC'],
        true,
        { gasLimit: ethers.BigNumber.from(1e6) }
    )
    console.log('setExchangeToken')

    await facets.marketbase_facet.attach(diamond.address).connect(owner).setComission(
        ethers.BigNumber.from(0), // comission type
        ethers.BigNumber.from(100), // 10%
        { gasLimit: ethers.BigNumber.from(1e6) }
    )
    console.log('setComission')

    await facets.gas_facet.attach(diamond.address).connect(owner).setRefillNativeValue(
        ethers.utils.parseEther(config.user_net_coin_refill_amount), // 10%
        { gasLimit: ethers.BigNumber.from(1e6) }
    )
    console.log('setRefillNativeValue')

    // ---------------- NFT ----------------------------------------------------------------------------

    console.log('       NFT                                                                             '.bgGrey.white.bold)
    console.log()

    let erc1155 = await DeployContract('ENFT', '', owner.address)

    await erc1155.connect(owner).setTokenURIPrefix(
        config.nft_token_uri_prefix,
        { gasLimit: ethers.BigNumber.from(1e6) }
    )
    await erc1155.connect(owner).setContractURI(
        config.nft_contract_uri_prefix + erc1155.address,
        { gasLimit: ethers.BigNumber.from(1e6) }
    )

    console.log('contract uri', (await erc1155.contractURI()).bgBlack.brightGreen.bold)
    console.log()

    // ---------------------------------------------------------------------------------------------------
    // ---------------- Runtime Config -------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------

    let out_config_str = JSON.stringify(config, null, 2)
    let out_config_path = path.join(out_config_dir, 'config.json')
    fs.writeFileSync(out_config_path, out_config_str, 'utf-8')
    console.log('saved config to', out_config_path.bgBlack.brightGreen.bold)
    out_config_path = path.join(out_config_dir, `config.${network_name}.json`)
    fs.writeFileSync(out_config_path, out_config_str, 'utf-8')
    console.log('saved config to', out_config_path.bgBlack.brightGreen.bold)
}

main()
    .then(() => process.exit(0))
    .catch(error => {
        console.error(error)
        process.exit(1)
    })
