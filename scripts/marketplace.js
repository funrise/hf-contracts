const fs = require('fs')
const path = require('path')
require('colors')

const { ethers, network } = require('hardhat');
const assert = require('assert');

async function main() {

    console.log()
    console.log('                                                                                       '.bgGrey.white.bold)
    console.log('       FunRise marketplace                                                             '.bgGrey.white.bold)
    console.log('                                                                                       '.bgGrey.white.bold)
    console.log()

    let params = {
        'FR_NETWORK': null,
        'FR_OWNER_PRIVATE_KEY': null,
        'FR_MARKETPLACE_ADDRESS': null,
        'FR_VALUE': null,
        'FR_PROVIDER_RPC': null,
        'FR_ACTION': null
    }
    Object.entries(params).map(([name, val]) => {
        params[name] = process.env[name] || ''
        console.log(name, params[name].bgBlack.brightYellow.bold)
    })
    Object.entries(params).map(([name, val]) => {
        if (name != 'FR_ACTION' || name != 'FR_PREVIEW') assert(!!params[name], name + ' is not defined')
    })
    console.log()
    assert(network.name == params['FR_NETWORK'], 'netowrk mismatch')

    const action = params['FR_ACTION']

    process.env.FR_PREVIEW = process.env.FR_PREVIEW.replace('0', '')

    let provider = new ethers.providers.JsonRpcProvider(params['FR_PROVIDER_RPC'])

    let owner = new ethers.Wallet(params['FR_OWNER_PRIVATE_KEY'], provider)
    console.log('owner address', owner.address.toString().bgBlack.brightYellow.bold)
    // console.log('owner balance', ethers.utils.formatEther(await owner.getBalance()).bgBlack.brightGreen.bold)
    console.log()

    if (action) {
        console.log()
    } else {
        return
    }

    let show_balances = async () => {
        console.log(market_address.bgBlack.brightYellow.bold, 'market balance', ethers.utils.formatEther(await provider.getBalance(market_address)).bgBlack.brightGreen.bold)
        console.log(owner.address.bgBlack.brightYellow.bold, 'owner balance', ethers.utils.formatEther(await provider.getBalance(owner.address)).bgBlack.brightGreen.bold)
        console.log()
    }

    let market_address = params['FR_MARKETPLACE_ADDRESS']

    await show_balances()

    let refill = async () => {

        console.log('REFILL'.bgBlack.brightYellow.bold)
        console.log()

        console.log(process.env.FR_PREVIEW)
        if (!process.env.FR_PREVIEW) {
            let tx = {
                nonce: await provider.getTransactionCount(owner.address),
                chainId: (await provider.getNetwork()).chainId,
                gasLimit: ethers.BigNumber.from(2e5),
                gasPrice: await provider.getGasPrice(),
                to: market_address,
                value: ethers.utils.parseEther(params['FR_VALUE'])
            }
            console.log('refill')
            tx = await owner.sendTransaction(tx)
            tx.wait()
        } else {
            console.log('preview')
        }

        await show_balances()
    }

    let withdraw = async () => {

        console.log('WITHDRAW', params['FR_VALUE'].bgBlack.brightYellow.bold)
        console.log()

        const artifacts_dir = path.resolve('artifacts/contracts')
        const abi = require(path.join(artifacts_dir, 'marketplace', 'facets', 'MarketBaseFacet.sol', 'MarketBaseFacet.json')).abi

        let market = new ethers.Contract(market_address, abi, provider).connect(owner)
        if (!process.env.FR_PREVIEW) {
            console.log('withdraw')
            await market.withdraw(owner.address, ethers.utils.parseEther(params['FR_VALUE']))
        } else {
            console.log('preview')
        }

        await show_balances()
    }

    let transfer_ownership = async () => {

        let new_owner_private_key = process.env.NEW_OWNER_PRIVATE_KEY
        if (!new_owner_private_key) {
            console.log('no new owner')   
            return
        }

        let new_owner = new ethers.Wallet(new_owner_private_key, provider)
        ethers.utils.getAddress(new_owner.address)
        console.log('TRANSFER', new_owner.address.bgBlack.brightYellow.bold)
        console.log()

        const artifacts_dir = path.resolve('artifacts/contracts')
        const abi = require(path.join(artifacts_dir, 'marketplace', 'facets', 'OwnershipFacet.sol', 'OwnershipFacet.json')).abi

        let market = new ethers.Contract(market_address, abi, provider).connect(owner)
        if (!process.env.FR_PREVIEW) {
            console.log('transfer')
            await market.transferOwnership(new_owner.address)
        } else {
            console.log('preview')
        }

        console.log('ownership transfered')
    }

    action == 'REFILL' && await refill()
    action == 'WITHDRAW' && await withdraw()
    action == 'TRANSFER' && await transfer_ownership()
}

main()
    .then(() => process.exit(0))
    .catch(error => {
        console.error(error)
        process.exit(1)
    })
