 #!/usr/bin/bash 

if [ -z "$FR_NETWORK" ] 
then
	echo "FR_NETWORK is not defined"
    exit 0
fi
if [ -z "$FR_OWNER_PRIVATE_KEY" ] 
then
	echo "FR_OWNER_PRIVATE_KEY is not defined"
    exit 0    
fi
if [ -z "$FR_INFURA_ID" ] 
then
	echo "FR_INFURA_ID is not defined"
    exit 0    
fi
if [ -z "$IPFS_PROJECT_ID" ] 
then
	echo "IPFS_PROJECT_ID is not defined"
    exit 0
fi
if [ -z "$IPFS_PROJECT_SECRET" ] 
then
	echo "IPFS_PROJECT_SECRET is not defined"
    exit 0
fi

echo "FR_NETWORK $FR_NETWORK"
echo "FR_OWNER_PRIVATE_KEY $FR_OWNER_PRIVATE_KEY"
echo "FR_INFURA_ID $FR_INFURA_ID"
echo "IPFS_PROJECT_ID $IPFS_PROJECT_ID"
echo "IPFS_PROJECT_SECRET $IPFS_PROJECT_SECRET"
echo ""

