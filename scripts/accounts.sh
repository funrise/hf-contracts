 #!/usr/bin/bash 

source $(dirname $0)/check.sh

export FR_PREVIEW=$1
export FR_NETWORK=$2
export FR_ACTION=$3
export FR_ACCOUNTS_NUM=$4
export FR_VALUE=$5
export FR_RECIPIENT_ADDRESS=$6

echo "args: FR_PREVIEW FR_NETWORK FR_ACTION FR_ACCOUNTS_NUM FR_VALUE FR_RECIPIENT_ADDRESS"
echo ""

if [ $FR_NETWORK == "matic_fork" ]
then
export FR_PROVIDER_RPC="http://0.0.0.0:8545"
elif [ $FR_NETWORK == "matic" ]
then
export FR_PROVIDER_RPC="https://polygon-mainnet.infura.io/v3/${FR_INFURA_ID}"
else
echo "bad network"
exit
fi
echo "set FR_PROVIDER_RPC to $FR_PROVIDER_RPC"

export FR_VALUE="${FR_VALUE:-NONE}"
export FR_RECIPIENT_ADDRESS="${FR_RECIPIENT_ADDRESS:-NONE}"

echo "FR_MARKETPLACE_ADDRESS $FR_MARKETPLACE_ADDRESS"
echo "FR_ACCOUNTS_NUM $FR_ACCOUNTS_NUM"
echo "FR_VALUE $FR_VALUE"
echo "FR_RECIPIENT_ADDRESS $FR_RECIPIENT_ADDRESS"
echo "FR_PROVIDER_RPC $FR_PROVIDER_RPC"
echo "FR_ACTION $FR_ACTION"
echo ""

sleep 1

npx hardhat run scripts/accounts.js --network $FR_NETWORK


