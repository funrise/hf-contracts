require("@nomiclabs/hardhat-waffle");

task("accounts", "Prints the list of accounts", async () => {
  const accounts = await ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address);
  }
});

let provider_rpc = ''

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  solidity: "0.8.0",
  mocha: {
    timeout: 4000000
  },
  networks: {
    matic_fork: {
      url: "http://127.0.0.1:8545",
    },
    matic: {
      url: provider_rpc,
    },
  },
  watcher: {
    compilation: {
      tasks: ["compile"],
    }
  }
};