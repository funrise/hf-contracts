// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/utils/cryptography/ECDSA.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

import "./IENFT.sol";
import "./EERC1155.sol";
import './ERC2981PerTokenRoyalties.sol';

import {LibStrings} from "../shared/libraries/LibStrings.sol";

contract ENFT is IENFT, Ownable, EERC1155, ERC2981PerTokenRoyalties {
    using SafeERC20 for IERC20;

    /*
    * bytes4(keccak256('contractURI()')) == 0xe8a3d485
    */
    bytes4 private constant _INTERFACE_CONTRACT_URI = 0xe8a3d485;
    string public contractURI;

    string public _tokenURIPrefix;

    mapping(uint256 => string) private _tokenURIs;
    mapping(uint256 => bool) public tokenIds;

    constructor(
        string memory uri,
        address owner
    ) EERC1155(uri) {

        transferOwnership(owner);
    }

    function setTokenURIPrefix(string memory newURI) public onlyOwner {

        _tokenURIPrefix = newURI;
    }

    function setContractURI(string memory contractURI_) public onlyOwner {

        contractURI = contractURI_;
    }

    function tokenURI(
        uint256 tokenId
    ) public view override returns (string memory) {

        return string(abi.encodePacked(_tokenURIPrefix, _tokenURIs[tokenId]));
    }

    function mint(
        address owner_,
        uint256 tokenId,
        uint256 supply,
        string memory tokenURI_,
        address royaltyRecipient,
        uint24 royaltyValue
    ) public override {

        require(
            owner_ == _msgSender() || owner() == _msgSender(),
            "ENFT: NOT_ALLOWED"
        );

        mintInternal(
            owner_,
            tokenId,
            supply,
            tokenURI_,
            royaltyRecipient,
            royaltyValue
        );
    }

    function mintInternal(
        address owner_,
        uint256 tokenId,
        uint256 supply,
        string memory tokenURI_,
        address royaltyRecipient,
        uint24 royaltyValue
    ) internal {

        require(
            supply > 0,
            "ENFT: ZERO_SUPPLY"
        );
        require(
            !_exists(tokenId),
            "ENFT: TOKENID_EXISTS"
        );

        _mint(
            owner_,
            tokenId,
            supply,
            ""
        );
        _tokenURIs[tokenId] = tokenURI_;
        tokenIds[tokenId] = true;

        if (royaltyValue > 0) {
            _setTokenRoyalty(tokenId, royaltyRecipient, royaltyValue);
        }

        emit URI(tokenURI_, tokenId);
    }

    function mintSigned(
        MintSignedRequest memory request
    ) public override {

        require(
            _verify(
                _hashMint(
                    request.owner,
                    request.tokenId,
                    request.supply,
                    request.tokenURI,
                    request.royaltyRecipient,
                    request.royaltyValue
                ),
                request.owner,
                request.signature
            ),
            "ENFT: INVALID_MINT_SIGNATURE"
        );

        mintInternal(
            request.owner,
            request.tokenId,
            request.supply,
            request.tokenURI,
            request.royaltyRecipient,
            request.royaltyValue
        );
    }

    function redeemSigned(
        RedeemSignedRequest memory request
    ) public override {

        require(
            _verify(
                _hashRedeem(
                    request.owner,
                    request.newOwner,
                    request.tokenId,
                    request.exchangeToken,
                    request.amount,
                    request.price
                ),
                request.owner,
                request.signature
            ),
            "ENFT: INVALID_REDEEM_SIGNATURE"
        );

        uint256 cost = request.price * request.amount;

        IERC20(request.exchangeToken).safeTransferFrom(request.newOwner, request.owner, cost);

        emit ApprovalForAll(request.owner, request.newOwner, true);

        _safeTransferFrom(
            request.owner,
            request.newOwner,
            request.tokenId,
            request.amount,
            ""
        );

        emit NFTAssetRedeemed(
            address(this),
            request.tokenId,
            request.owner,
            request.newOwner,
            request.exchangeToken,
            request.price,
            request.amount
        );

        emit ApprovalForAll(request.owner, request.newOwner, false);
    }

    function mintRedeemSigned(
        MintRedeemSignedRequest calldata request
    ) public override {

        MintSignedRequest memory mintParams = MintSignedRequest(
            request.owner,
            request.tokenId,
            request.supply,
            request.tokenURI,
            request.royaltyRecipient,
            request.royaltyValue,
            request.mintSignature
        );

        mintSigned(
            mintParams
        );

        RedeemSignedRequest memory redeemParms = RedeemSignedRequest(
            request.owner,
            request.newOwner,
            request.tokenId,
            request.exchangeToken,
            request.amount,
            request.price,
            request.redeemSignature
        );

        redeemSigned(
            redeemParms
        );        
    }

    function _exists(uint256 tokenId) internal view returns (bool) {
        return tokenIds[tokenId];
    }

    function _hashMint(
        address owner_,
        uint256 tokenId,
        uint256 supply,
        string memory tokenURI_,
        address royaltyRecipient,
        uint24 royaltyValue
    ) internal view returns (bytes32) {

        return ECDSA.toEthSignedMessageHash(
            keccak256(
                abi.encodePacked(
                    owner_,
                    tokenId,
                    supply,
                    tokenURI_,
                    royaltyRecipient,
                    royaltyValue,
                    block.chainid
                )
            )
        );
    }

    function _hashRedeem(
        address owner_,
        address newOwner,
        uint256 tokenId,
        address exchangeToken,
        uint256 amount,
        uint256 price
    ) internal view returns (bytes32) {

        return ECDSA.toEthSignedMessageHash(
            keccak256(
                abi.encodePacked(
                    owner_,
                    newOwner,
                    tokenId,
                    exchangeToken,
                    amount,
                    price,
                    block.chainid
                )
            )
        );
    }

    function _verify(
        bytes32 digest,
        address signer,
        bytes memory signature
    ) internal pure returns (bool) {

        return signer == ECDSA.recover(digest, signature);
    }

    function burn(
        address account,
        uint256 id,
        uint256 value
    ) public virtual {
        require(
            account == _msgSender() || isApprovedForAll(account, _msgSender()),
            "ENFT: NOT_ALLOWED"
        );

        tokenIds[id] = false;

        _burn(account, id, value);
    }

    function burnBatch(
        address account,
        uint256[] memory ids,
        uint256[] memory values
    ) public virtual {
        require(
            account == _msgSender() || isApprovedForAll(account, _msgSender()),
            "ENFT: NOT_ALLOWED"
        );

        for (uint256 i = 0; i < ids.length; i++) {
            uint256 id = ids[i];
            tokenIds[id] = false;
        }

        _burnBatch(account, ids, values);
    }

    function supportsInterface(
        bytes4 interfaceId
    ) public view override(IERC165, EERC1155, ERC2981Base) returns (bool) {

        return  interfaceId == _INTERFACE_CONTRACT_URI || super.supportsInterface(interfaceId);
    }
}