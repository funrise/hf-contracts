// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC1155/IERC1155.sol";

interface IENFT is IERC1155 {

    struct MintSignedRequest {
        address  owner;
        uint256 tokenId;
        uint256 supply;
        string tokenURI;
        address royaltyRecipient;
        uint24 royaltyValue;
        bytes signature;
    }

    struct RedeemSignedRequest {
        address owner;
        address newOwner;
        uint256 tokenId;
        address exchangeToken;
        uint256 amount;
        uint256 price;
        bytes signature;
    }

    struct MintRedeemSignedRequest {
        address owner;
        uint256 tokenId;
        uint256 supply;
        string tokenURI;
        address royaltyRecipient;
        uint24 royaltyValue;
        address newOwner;
        address exchangeToken;
        uint256 amount;
        uint256 price;
        bytes mintSignature;
        bytes redeemSignature;
    }

    event NFTAssetRedeemed(
        address contract_address,
        uint256 indexed id,
        address indexed owner,
        address indexed recipient,
        address exchangeToken,
        uint256 price,
        uint256 amount
    );

    function tokenURI(
        uint256 _tokenId
    ) external view returns (string memory);

    function mint(
        address _owner,
        uint256 _tokenId,
        uint256 _supply,
        string memory _tokenURI,
        address _royaltyRecipient,
        uint24 _royaltyValue
    ) external;

    function mintSigned(
        MintSignedRequest memory _request
    ) external;

    function redeemSigned(
        RedeemSignedRequest memory _request
    ) external;

    function mintRedeemSigned(
        MintRedeemSignedRequest calldata _request
    ) external;
}