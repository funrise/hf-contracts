// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import {AppStorage, Asset, Bid, Modifiers} from "../libraries/LibAppStorage.sol";
import {LibAuction, AuctionListOrder, AuctionBidOrder, AuctionResolveOrder} from "../libraries/LibAuction.sol";

contract AuctionFacet is Modifiers {
    AppStorage internal s;

    event AuctionAssetListed(
        address indexed owner,
        address indexed token,
        address exchangeToken,
        uint256 indexed id,
        uint256 amount,
        uint256 startingPrice,
        uint256 reservePrice,
        uint64 listingTime,
        uint64 duration,
        uint8 listingType,
        uint8 state
    );

    event AuctionBidMade(
        address indexed owner,
        address bidder,
        address indexed token,
        uint256 indexed id,
        uint256 value,
        uint64 listingTime
    );

    event AuctionBidReturned(
        address indexed owner,
        address bidder,
        address indexed token,
        uint256 indexed id,
        uint256 value,
        uint64 listingTime
    );

    event AuctionResolved(
        address indexed owner,
        address bidder,
        address indexed token,
        uint256 indexed id,
        uint256 value,
        uint64 listingTime,
        uint8 result
    );

    event RoyaltyPayed(
        address indexed token,
        address exchangeToken,
        address buyer,
        address indexed receiver,
        uint256 indexed tokenId,
        uint256 value
    );

    event ComissionPayed(
        address indexed token,
        address exchangeToken,
        address buyer,
        address indexed receiver,
        uint256 indexed tokenId,
        uint256 value,
        uint8 comissionType
    );
    
    function auctionSetMinReservePrice(
        address _exchangeToken,
        uint256 _minPrice
    ) external onlyDiamondOwner {

        s.minReservePriceTotal[_exchangeToken] = _minPrice;
    }

    function auctionSetDurationLimits(
        uint256 _minDuration,
        uint256 _maxDuration
    ) external onlyDiamondOwner {

        s.minAuctionDuration = _minDuration;
        s.maxAuctionDuration = _maxDuration;
    }

    function auctionListAsset(
        AuctionListOrder calldata _listOrder
    ) public isSender(_listOrder.owner) returns (uint256) {

        Asset storage asset = s.assets[_listOrder.id][_listOrder.owner][_listOrder.token];

        LibAuction.checkAuctionListOrder(
            asset,
            _listOrder
        );

        return LibAuction.applyAuctionListOrder(
            asset,
            _listOrder
        );
    }

    function auctionMakeBid(
        AuctionBidOrder calldata _bidOrder
    ) public isSender(_bidOrder.bidder) {

        Asset storage asset = s.assets[_bidOrder.id][_bidOrder.owner][_bidOrder.token];
        Bid storage bid = s.bids[_bidOrder.id][_bidOrder.owner][_bidOrder.token];

        LibAuction.checkAuctionBidOrder(
            asset,
            bid,
            _bidOrder
        );

        LibAuction.applyAuctionBidOrder(
            asset,
            bid,
            _bidOrder
        );
    }

    function auctionResolve(
        AuctionResolveOrder calldata _resolveOrder
    ) public {

        LibAuction.resolveAuction(
            _resolveOrder
        );
    }


}
