// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import {AppStorage, Asset, Modifiers} from "../libraries/LibAppStorage.sol";
import {LibFixedPrice, FixedPriceListOrder, FixedPriceUnlistOrder, FixedPriceRedeemOrder} from "../libraries/LibFixedPrice.sol";

contract FixedPriceFacet is Modifiers {
    AppStorage internal s;

    event FixedPriceAssetListed(
        address indexed owner,
        address indexed token,
        address exchangeToken,
        uint256 indexed id,
        uint256 amount,
        uint256 price,
        uint64 listingTime,
        uint8 state
    );

    event FixedPriceAssetUnlisted(
        address indexed owner,
        address indexed token,
        uint256 indexed id,
        uint64 listingTime,
        uint8 state
    );

    event FixedPriceAssetRedeemed(
        address indexed owner,
        address buyer,
        address indexed token,
        uint256 indexed id,
        uint256 amount,
        uint256 price,
        uint64 listingTime,
        uint8 state
    );

    event RoyaltyPayed(
        address indexed token,
        address exchangeToken,
        address buyer,
        address indexed receiver,
        uint256 indexed tokenId,
        uint256 value
    );

    event ComissionPayed(
        address indexed token,
        address exchangeToken,
        address buyer,
        address indexed receiver,
        uint256 indexed tokenId,
        uint256 value,
        uint8 comissionType
    );
    
    function fixedPriceListAsset(
        FixedPriceListOrder calldata _listOrder
    ) public isSender(_listOrder.owner) returns (uint256) {

        Asset storage asset = s.assets[_listOrder.id][_listOrder.owner][_listOrder.token];

        LibFixedPrice.checkFixedPriceListOrder(
            asset,
            _listOrder
        );

        return LibFixedPrice.applyFixedPriceListOrder(
            asset,
            _listOrder
        );
    }

    function fixedPriceUnlistAsset(
        FixedPriceUnlistOrder calldata _unlistOrder
    ) public isSender(_unlistOrder.owner) {

        Asset storage asset = s.assets[_unlistOrder.id][_unlistOrder.owner][_unlistOrder.token];

        LibFixedPrice.checkFixedPriceUnlistOrder(
            asset,
            _unlistOrder
        );

        LibFixedPrice.fixedPriceUnlist(
            asset,
            _unlistOrder
        );
    }

    function fixedPriceRedeemAsset(
        FixedPriceRedeemOrder calldata _redeemOrder
    ) public isSender(_redeemOrder.buyer) notEqual(_redeemOrder.buyer, _redeemOrder.owner) {

        Asset storage asset = s.assets[_redeemOrder.id][_redeemOrder.owner][_redeemOrder.token];

        LibFixedPrice.checkFixedPriceRedeemOrder(
            asset,
            _redeemOrder
        );

        LibFixedPrice.fixedPriceRedeem(
            asset,
            _redeemOrder
        );
    }
}
