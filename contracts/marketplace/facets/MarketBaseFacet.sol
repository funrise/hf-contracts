// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import {AppStorage, Modifiers, Asset} from "../libraries/LibAppStorage.sol";

import {IERC20} from "../../shared/interfaces/IERC20.sol";
import {LibCommon} from "../../shared/libraries/LibCommon.sol";
import {LibDiamond} from "../../shared/libraries/LibDiamond.sol";
import {LibECDSA} from "../../shared/libraries/LibECDSA.sol";

contract MarketBaseFacet is Modifiers {
    AppStorage internal s;

    receive() external payable {}

    function getListedAsset(
        address _owner,
        address _token,
        uint256 _id
    ) public view returns (Asset memory) {
        
        return s.assets[_id][_owner][_token];
    }

    function setExchangeToken(
        address _exchangeToken,
        bool _isAllowed
    ) external onlyDiamondOwner {

        s.exchangeTokens[_exchangeToken] = _isAllowed;
    }

    function setComission(
        uint8 _type,
        uint24 _percentage
     ) external onlyDiamondOwner {

        s.comissions[_type] = _percentage;
    }

    function withdraw(
        address _recipient,
        uint256 _amount
    ) external onlyDiamondOwner {

        payable(_recipient).transfer(_amount);
    }

    function withdrawSigned(
        address _recipient,
        uint256 _amount,
        bytes memory _signature
    ) external {

        require(
            LibDiamond.contractOwner() == LibECDSA.recover(
                LibECDSA.toEthSignedMessageHash(
                    keccak256(
                        abi.encodePacked(
                            _recipient,
                            _amount,
                            block.chainid
                        )
                    )
                ),
                _signature
            ), "BaseFacet: INVALID_SIGNATURE"
        );

        payable(_recipient).transfer(_amount);
    }

    function withdrawERC20(
        address _recipient,
        address _token,
        uint256 _amount
    ) external onlyDiamondOwner {

        IERC20 exchangeToken = IERC20(_token);

        exchangeToken.transfer(_recipient, _amount);
    }

    function withdrawERC20Signed(
        address _recipient,
        address _token,
        uint256 _amount,
        bytes memory _signature
    ) external {

        require(
            LibDiamond.contractOwner() == LibECDSA.recover(
                LibECDSA.toEthSignedMessageHash(
                    keccak256(
                        abi.encodePacked(
                            _recipient,
                            _token,
                            _amount,
                            block.chainid
                        )
                    )
                ),
                _signature
            ), "BaseFacet: INVALID_SIGNATURE"
        );

        IERC20 exchangeToken = IERC20(_token);

        exchangeToken.transfer(_recipient, _amount);
    }

    function onERC1155Received(
        address,
        address,
        uint256,
        uint256,
        bytes memory
    ) public virtual returns (bytes4) {
        return this.onERC1155Received.selector;
    }

    function onERC1155BatchReceived(
        address,
        address,
        uint256[] memory,
        uint256[] memory,
        bytes memory
    ) public virtual returns (bytes4) {
        return this.onERC1155BatchReceived.selector;
    }
}
