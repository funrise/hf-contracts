// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import {AppStorage, Modifiers} from "../libraries/LibAppStorage.sol";
import {LibCommon} from "../../shared/libraries/LibCommon.sol";

contract GasFacet is Modifiers {
    AppStorage internal s;

    function setRoot(
        bytes32 _root,
        bool _enabled
    ) external onlyDiamondOwner {

        s.roots[_root] = _enabled;
    }

    function setRefillNativeValue(
        uint256 _value
    ) external onlyDiamondOwner {

        s.refillNativeValue = _value;
    }

    function refillNative(
        bytes32[] calldata _proof,
        bytes32 _root
    ) public {

        address _account = LibCommon.msgSender();

        require(
            s.roots[_root],
            "GasFacet: INVALID_ROOT"
        );

        require(
            LibCommon.verifyLeaf(
                keccak256(abi.encodePacked(_account)),
                _proof,
                _root
            ),
            "GasFacet: INVALID_PROOF"
        );

        (bool sent, bytes memory data) = payable(_account).call{value: s.refillNativeValue}(""); 
        require(
            sent,
            "GasFacet: SEND_CALL_FAILED"
        );
    }
}
