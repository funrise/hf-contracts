// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";

import {LibAppStorage, AppStorage, Asset, Bid, ListingType, AssetState, ComissionType} from "../libraries/LibAppStorage.sol";
import {LibMarketplace} from "../libraries/LibMarketplace.sol";

import {LibDiamond} from "../../shared/libraries/LibDiamond.sol";
import {LibCommon} from "../../shared/libraries/LibCommon.sol";
import {IERC1155} from "../../shared/interfaces/IERC1155.sol";

struct AuctionListOrder {
    address owner;              // asset owner
    address token;              // nft contract address
    address exchangeToken;      // payment token
    uint256 id;                 // token id
    uint256 amount;             // erc1155 token amount
    uint256 startingPrice;      // bids below this value are rejectd
    uint256 reservePrice;       // do not auto sell if final highest bid is below this value
    uint64 duration;            // auction dutation in seconds
}

struct AuctionBidOrder {
    address owner;              // asset owner
    address bidder;             // buyer and recipient
    address token;              // nft contract address
    address exchangeToken;      // payment token
    uint256 id;                 // token id
    uint256 value;              // total bid value
    uint64 listingTime;         // asset listing time
}

struct AuctionResolveOrder {
    address owner;              // asset owner
    address token;              // nft contract address
    uint256 id;                 // token id
    bool acceptHighestBid;      // accept the highest bid / close the auction and return assets
}

enum AuctionResult { AuctionCancelled, BidAccepted }

library LibAuction {

    using SafeERC20 for IERC20;

    event AuctionAssetListed(
        address indexed owner,
        address indexed token,
        address exchangeToken,
        uint256 indexed id,
        uint256 amount,
        uint256 startingPrice,
        uint256 reservePrice,
        uint64 listingTime,
        uint64 duration,
        uint8 listingType,
        uint8 state
    );

    event AuctionBidMade(
        address indexed owner,
        address bidder,
        address indexed token,
        uint256 indexed id,
        uint256 value,
        uint64 listingTime
    );

    event AuctionBidReturned(
        address indexed owner,
        address bidder,
        address indexed token,
        uint256 indexed id,
        uint256 value,
        uint64 listingTime
    );

    event AuctionResolved(
        address indexed owner,
        address bidder,
        address indexed token,
        uint256 indexed id,
        uint256 value,
        uint64 listingTime,
        uint8 result
    );

    function checkAuctionListOrder (
        Asset storage asset,
        AuctionListOrder calldata _listOrder
    ) internal view {

        AppStorage storage s = LibAppStorage.diamondStorage();

        LibMarketplace.checkAssetState(asset, AssetState.Idle);
        LibMarketplace.checkAssetExchangeSupport(_listOrder.exchangeToken);

        require(
            _listOrder.startingPrice > 0, 
            "Acution: ZERO_PRICE"
        );
        require(
            _listOrder.amount > 0, 
            "Acution: ZERO_AMOUNT"
        );
        if (_listOrder.reservePrice > 0) {
            if (_listOrder.reservePrice * _listOrder.amount < s.minReservePriceTotal[_listOrder.exchangeToken] ||
                _listOrder.reservePrice <= _listOrder.startingPrice) {
                revert("Acution: BAD_RESERVE_PRICE");
            }
        }
        if (s.minAuctionDuration > 0 && s.minAuctionDuration > _listOrder.duration || 
            s.maxAuctionDuration > 0 && s.maxAuctionDuration <= _listOrder.duration) {
            revert("Acution: BAD_DURATION");
        }
    }

    function checkAuctionBidOrder(
        Asset storage asset,
        Bid storage currentBid,
        AuctionBidOrder calldata _bidOrder
    ) internal view {

        LibMarketplace.checkAssetListingType(asset, ListingType.Auction);
        LibMarketplace.checkAssetState(asset, AssetState.OnSale);
        LibMarketplace.checkAssetExchangeMatch(asset, _bidOrder.exchangeToken);
        LibMarketplace.checkAssetListingTimeMatch(asset, _bidOrder.listingTime);

        require(
            asset.listingTime + asset.duration > block.timestamp,
            "Acution: EXPIRED"
        );

        if (currentBid.active) {
            if (currentBid.bidder == _bidOrder.bidder) {
                revert("Acution: BID_IS_ACTIVE");
            }
            if (currentBid.value >= _bidOrder.value) {
                revert("Acution: LOW_BID_VALUE");
            }
        } else {
            if (asset.price * asset.amount > _bidOrder.value) {
                revert("Acution: LOW_BID_VALUE");
            }
        }
    }

    function applyAuctionListOrder (
        Asset storage asset,
        AuctionListOrder calldata _listOrder
    ) internal returns (uint64) {

        IERC1155(_listOrder.token).safeTransferFrom(
            _listOrder.owner,
            address(this),
            _listOrder.id,
            _listOrder.amount,
            ""
        );

        asset.exchangeToken = _listOrder.exchangeToken;
        asset.amount = _listOrder.amount;
        asset.price = _listOrder.startingPrice;
        asset.reservePrice = _listOrder.reservePrice;
        asset.listingTime = uint64(block.timestamp);
        asset.duration = _listOrder.duration;
        asset.listingType = uint8(ListingType.Auction);
        asset.state =  uint8(AssetState.OnSale);

        emit AuctionAssetListed(
            _listOrder.owner,
            _listOrder.token,
            asset.exchangeToken,
            _listOrder.id,
            asset.amount,
            asset.price,
            asset.reservePrice, 
            asset.listingTime,
            asset.duration, 
            asset.listingType,
            asset.state
        );

        return asset.listingTime;
    }

    function applyAuctionBidOrder(
        Asset storage asset,
        Bid storage bid,
        AuctionBidOrder calldata _bidOrder
    ) internal {

        IERC20(asset.exchangeToken).transferFrom(_bidOrder.bidder, address(this), _bidOrder.value);

        if (bid.active) {
            returnBid(
                bid,
                _bidOrder.owner,
                _bidOrder.token,
                _bidOrder.id,
                asset.listingTime
            );
        }

        bid.bidder = _bidOrder.bidder;
        bid.exchangeToken = asset.exchangeToken;
        bid.value = _bidOrder.value;
        bid.active = true;

        emit AuctionBidMade (
            _bidOrder.owner,
            bid.bidder,
            _bidOrder.token,
            _bidOrder.id,
            bid.value,
            asset.listingTime
        );
    }

    function returnBid(
        Bid storage _bid,
        address _owner,
        address _token,
        uint256 _id,
        uint64 _listingTime
    ) internal {

        IERC20(_bid.exchangeToken).transfer(_bid.bidder, _bid.value);

        emit AuctionBidReturned (
            _owner,
            _bid.bidder,
            _token,
            _id,
            _bid.value,
            _listingTime
        );
    }

    function execute(
        Asset storage _asset,
        Bid storage _bid,
        address _owner,
        address _token,
        uint256 _id,
        bool _acceptBid
    ) internal {

        AuctionResult result;

        if (_bid.active) {

            if (_acceptBid) {

                uint256 royaltyPayed = LibMarketplace.payRoyalty(
                    _token,
                    _asset.exchangeToken,
                    address(this),
                    _bid.bidder,
                    _id,
                    _bid.value
                );

                uint256 comissionPayed = LibMarketplace.payComission(
                    ComissionType.Universal,
                    _token,
                    _asset.exchangeToken,
                    address(this),
                    _bid.bidder,
                    _id,
                    _bid.value
                );

                IERC20(_asset.exchangeToken).transfer(_owner, _bid.value - royaltyPayed - comissionPayed);

                IERC1155(_token).safeTransferFrom(
                    address(this),
                    _bid.bidder,
                    _id,
                    _asset.amount,
                    ""
                );

                result = AuctionResult.BidAccepted;
            } else {

                returnBid(
                    _bid,
                    _owner,
                    _token,
                    _id,
                    _asset.listingTime
                );

                IERC1155(_token).safeTransferFrom(
                    address(this),
                    _owner,
                    _id,
                    _asset.amount,
                    ""
                );

                result = AuctionResult.AuctionCancelled;
            }

            _bid.active = false;
        } else {

            IERC1155(_token).safeTransferFrom(
                address(this),
                _owner,
                _id,
                _asset.amount,
                ""
            );

            result = AuctionResult.AuctionCancelled;
        }

        _asset.state = uint8(AssetState.Idle);

        emit AuctionResolved (
            _owner,
            _bid.bidder,
            _token,
            _id,
            _bid.value,
            _asset.listingTime,
            uint8(result)
        );
    }

    function resolveAuction(
        AuctionResolveOrder calldata _resolveOrder
    ) internal {

        AppStorage storage s = LibAppStorage.diamondStorage();

        address sender = LibCommon.msgSender();
        bool senderIsOwner = sender == _resolveOrder.owner;
        bool senderIsOperator = sender == LibDiamond.diamondStorage().contractOwner;

        Asset storage asset = s.assets[_resolveOrder.id][_resolveOrder.owner][_resolveOrder.token];
        Bid storage bid = s.bids[_resolveOrder.id][_resolveOrder.owner][_resolveOrder.token];

        LibMarketplace.checkAssetListingType(asset, ListingType.Auction);
        LibMarketplace.checkAssetState(asset, AssetState.OnSale);

        bool expired = block.timestamp > asset.listingTime + asset.duration;

        require(
            senderIsOwner || senderIsOperator,
            "Acution: NOT_ALLOWED"
        );

        if (senderIsOwner) {
            execute(
                asset,
                bid,
                _resolveOrder.owner,
                _resolveOrder.token,
                _resolveOrder.id,
                _resolveOrder.acceptHighestBid
            );
        }

        if (senderIsOperator && expired && bid.value >= asset.reservePrice * asset.amount) {
            execute(
                asset,
                bid,
                _resolveOrder.owner,
                _resolveOrder.token,
                _resolveOrder.id,
                true
            );
        }
    }
}
