// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import {LibAppStorage, AppStorage, Asset, Bid, ListingType, AssetState, ComissionType} from "../libraries/LibAppStorage.sol";

import {LibDiamond} from "../../shared/libraries/LibDiamond.sol";
import {LibCommon} from "../../shared/libraries/LibCommon.sol";
import {LibERC165Checker} from "../../shared/libraries/LibERC165Checker.sol";
import {IERC20} from "../../shared/interfaces/IERC20.sol";
import {IERC1155} from "../../shared/interfaces/IERC1155.sol";
import {IERC2981Royalties} from "../../tokens/IERC2981Royalties.sol";

library LibMarketplace {

    event RoyaltyPayed(
        address indexed token,
        address exchangeToken,
        address buyer,
        address indexed receiver,
        uint256 indexed tokenId,
        uint256 value
    );

    event ComissionPayed(
        address indexed token,
        address exchangeToken,
        address buyer,
        address indexed receiver,
        uint256 indexed tokenId,
        uint256 value,
        uint8 comissionType
    );

    function checkAssetListingType(
        Asset storage _asset,
        ListingType _listingType
    ) internal view {

        require(
            _asset.listingType == uint8(_listingType), 
            "Marketplace: WRONG_LISTRING_TYPE"
        );
    }

    function checkAssetState(
        Asset storage _asset,
        AssetState _state
    ) internal view {

        require(
            _asset.state == uint8(_state),
            "Marketplace: WRONG_ASSET_STATE"
        );
    }

    function checkAssetExchangeMatch(
        Asset storage _asset,
        address _token
    ) internal view {

        require(
            _asset.exchangeToken == _token, 
            "Marketplace: WRONG_EXCHANGE_TOKEN"
        );
    }

    function checkAssetListingTimeMatch(
        Asset storage _asset,
        uint64 _listingTime
    ) internal view {

        require(
            _asset.listingTime == _listingTime, 
            "Marketplace: liSTING_TIME_MISMATCH"
        );
    }

    function checkAssetExchangeSupport(
        address _token
    ) internal view {

        require(
           LibAppStorage.diamondStorage().exchangeTokens[_token], 
            "Marketplace: EXCHANGE_NOT_SUPPORED"
        );
    }

    function checkNonZero(
        address _address
    ) internal pure {

        require(
            _address != address(0), 
            "Marketplace: ZERO_ADDRESS"
        );
    }

    function payRoyalty(
        address _token,
        address _exchangeToken,
        address _payer,
        address _buyer,
        uint256 _tokenId,
        uint256 _value
    ) internal returns (uint256) {

        uint256 royaltyValue = 0;

        if (LibERC165Checker._supportsInterface(_token, 0x2a55205a)) { // bytes4(keccak256("royaltyInfo(uint256,uint256)")) == 0x2a55205a
            address royaltyReceiver;
            (royaltyReceiver, royaltyValue) = IERC2981Royalties(_token).royaltyInfo(_tokenId, _value);

            if (royaltyValue > 0 && royaltyValue < _value) {

                if (_payer == address(this)) {
                    IERC20(_exchangeToken).transfer(royaltyReceiver, royaltyValue);
                } else {
                    IERC20(_exchangeToken).transferFrom(_payer, royaltyReceiver, royaltyValue);
                }

                emit RoyaltyPayed(
                    _token,
                    _exchangeToken,
                    _buyer,
                    royaltyReceiver,
                    _tokenId,
                    royaltyValue
                );
            } else {
                royaltyValue = 0;
            }
        }

        return royaltyValue;
    }

    function payComission(
        ComissionType _type,
        address _token,
        address _exchangeToken,
        address _payer,
        address _buyer,
        uint256 _tokenId,
        uint256 _value
    ) internal returns (uint256) {

        uint256 comissionValue = (_value * LibAppStorage.diamondStorage().comissions[uint8(_type)]) / 10000;

        if (comissionValue > 0) {

            if (_payer != address(this)) {
                IERC20(_exchangeToken).transferFrom(_payer, address(this), comissionValue);
            }

            emit ComissionPayed(
                _token,
                _exchangeToken,
                _buyer,
                address(this),
                _tokenId,
                comissionValue,
                uint8(_type)
            );
        }

        return comissionValue;
    }
}
