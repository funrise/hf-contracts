// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";

import {LibAppStorage, AppStorage, Asset, Bid, ListingType, AssetState, ComissionType} from "../libraries/LibAppStorage.sol";
import {LibMarketplace} from "../libraries/LibMarketplace.sol";

import {LibDiamond} from "../../shared/libraries/LibDiamond.sol";
import {LibCommon} from "../../shared/libraries/LibCommon.sol";
import {IERC1155} from "../../shared/interfaces/IERC1155.sol";

struct FixedPriceListOrder {
    address owner;              // asset owner
    address token;              // nft contract address
    address exchangeToken;      // payment token
    uint256 id;                 // token id
    uint256 amount;             // erc1155 token amount
    uint256 price;              // total asset price
}

struct FixedPriceUnlistOrder {
    address owner;              // asset owner
    address token;              // nft contract address
    uint256 id;                 // token id
}

struct FixedPriceRedeemOrder {
    address owner;              // asset owner
    address buyer;              // payer and recipient
    address token;              // nft contract address
    address exchangeToken;      // payment token
    uint256 id;                 // token id
    uint256 amount;             // erc1155 token amount
    uint64 listingTime;         // asset listing time
}

library LibFixedPrice {

    using SafeERC20 for IERC20;

    event FixedPriceAssetListed(
        address indexed owner,
        address indexed token,
        address exchangeToken,
        uint256 indexed id,
        uint256 amount,
        uint256 price,
        uint64 listingTime,
        uint8 state
    );

    event FixedPriceAssetUnlisted(
        address indexed owner,
        address indexed token,
        uint256 indexed id,
        uint64 listingTime,
        uint8 state
    );

    event FixedPriceAssetRedeemed(
        address indexed owner,
        address buyer,
        address indexed token,
        uint256 indexed id,
        uint256 amount,
        uint256 price,
        uint64 listingTime,
        uint8 state
    );

    function emitFixedPriceAssetListed(
        Asset storage _asset,
        FixedPriceListOrder calldata _listOrder
    ) internal {

        emit FixedPriceAssetListed(
            _listOrder.owner,
            _listOrder.token,
            _asset.exchangeToken,
            _listOrder.id,
            _asset.amount,
            _asset.price,
            _asset.listingTime,
            _asset.state
        );
    }

    function emitFixedPriceAssetUnlisted(
        Asset storage _asset,
        FixedPriceUnlistOrder calldata _unlistOrder
    ) internal {
        
        emit FixedPriceAssetUnlisted(
            _unlistOrder.owner,
            _unlistOrder.token,
            _unlistOrder.id,
            _asset.listingType,
            _asset.state
        );
    }

    function emitFixedPriceAssetRedeemed(
        Asset storage _asset,
        FixedPriceRedeemOrder calldata _redeemOrder
    ) internal {

        emit FixedPriceAssetRedeemed(
            _redeemOrder.owner,
            _redeemOrder.buyer,
            _redeemOrder.token,
            _redeemOrder.id,
            _redeemOrder.amount,
            _asset.price,
            _asset.listingType,
            _asset.state
        );
    }

    function checkFixedPriceListOrder(
        Asset storage _asset,
        FixedPriceListOrder calldata _listOrder
    ) internal view {

        LibMarketplace.checkAssetState(_asset, AssetState.Idle);
        LibMarketplace.checkAssetExchangeSupport(_listOrder.exchangeToken);

        require(
            _listOrder.price > 0, 
            "FixedPrice: ZERO_PRICE"
        );
        require(
            _listOrder.amount > 0, 
            "FixedPrice: ZERO_AMOUNT"
        );
    }

    function applyFixedPriceListOrder(
        Asset storage _asset,
        FixedPriceListOrder calldata _listOrder
    ) internal returns (uint64) {

        IERC1155(_listOrder.token).safeTransferFrom(
            _listOrder.owner,
            address(this),
            _listOrder.id,
            _listOrder.amount,
            ""
        );

        _asset.exchangeToken = _listOrder.exchangeToken;
        _asset.amount = _listOrder.amount;
        _asset.price = _listOrder.price;
        _asset.reservePrice = 0;
        _asset.listingTime = uint64(block.timestamp);
        _asset.duration = 0;
        _asset.listingType = uint8(ListingType.FixedPrice);
        _asset.state =  uint8(AssetState.OnSale);

        LibFixedPrice.emitFixedPriceAssetListed(
            _asset,
            _listOrder
        );

        return _asset.listingTime;
    }

    function checkFixedPriceUnlistOrder(
        Asset storage _asset,
        FixedPriceUnlistOrder calldata _unlistOrder
    ) internal view {

        LibMarketplace.checkNonZero(_unlistOrder.owner);
        LibMarketplace.checkAssetListingType(_asset, ListingType.FixedPrice);
        LibMarketplace.checkAssetState(_asset, AssetState.OnSale);
    }

    function fixedPriceUnlist(
        Asset storage _asset,
        FixedPriceUnlistOrder calldata _unlistOrder
    ) internal {

        IERC1155(_unlistOrder.token).safeTransferFrom(
            address(this),
            _unlistOrder.owner,
            _unlistOrder.id,
            _asset.amount,
            ""
        );

        _asset.state = uint8(AssetState.Idle);

        emitFixedPriceAssetUnlisted(
            _asset,
            _unlistOrder
        );
    }

    function checkFixedPriceRedeemOrder(
        Asset storage _asset,
        FixedPriceRedeemOrder calldata _redeemOrder
    ) internal view {

        LibMarketplace.checkNonZero(_redeemOrder.owner);
        LibMarketplace.checkNonZero(_redeemOrder.buyer);
        LibMarketplace.checkAssetListingType(_asset, ListingType.FixedPrice);
        LibMarketplace.checkAssetState(_asset, AssetState.OnSale);
        LibMarketplace.checkAssetExchangeMatch(_asset, _redeemOrder.exchangeToken);
        LibMarketplace.checkAssetListingTimeMatch(_asset, _redeemOrder.listingTime);

        require(
            _redeemOrder.amount > 0 && _redeemOrder.amount <= _asset.amount,
            "FixedPrice: INVALID_AMOUNT"
        );
    }

    function fixedPriceRedeem(
        Asset storage _asset,
        FixedPriceRedeemOrder calldata _redeemOrder
    ) internal {

        uint256 cost = _asset.price * _redeemOrder.amount;

        require(
            IERC20(_asset.exchangeToken).allowance(_redeemOrder.buyer, address(this)) >= cost,
            "FixedPrice: INSUFFICIENT_ALLOWANCE"
        );

        uint256 royaltyPayed = LibMarketplace.payRoyalty(
            _redeemOrder.token,
            _asset.exchangeToken,
            _redeemOrder.buyer,
            _redeemOrder.buyer,
            _redeemOrder.id,
            cost
        );

        uint256 comissionPayed = LibMarketplace.payComission(
            ComissionType.Universal,
            _redeemOrder.token,
            _asset.exchangeToken,
            _redeemOrder.buyer,
            _redeemOrder.buyer,
            _redeemOrder.id,
            cost
        );

        cost = cost - royaltyPayed - comissionPayed;

        IERC20(_asset.exchangeToken).transferFrom(_redeemOrder.buyer, _redeemOrder.owner, cost);

        IERC1155(_redeemOrder.token).safeTransferFrom(
            address(this),
            _redeemOrder.buyer,
            _redeemOrder.id,
            _redeemOrder.amount,
            ""
        );

        _asset.amount = _asset.amount - _redeemOrder.amount;

        if (_asset.amount <= 0) {
            _asset.state = uint8(AssetState.Idle);
        }
 
        emitFixedPriceAssetRedeemed(
            _asset,
            _redeemOrder
        );
    }
}
