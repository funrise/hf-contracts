// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import {LibDiamond} from "../../shared/libraries/LibDiamond.sol";
import {LibCommon} from "../../shared/libraries/LibCommon.sol";

struct Asset {
    address exchangeToken;      // payment token
    uint256 amount;             // amount of ERC1155 token
    uint256 price;              // fixed price / starting price
    uint256 reservePrice;       // do not sell if the highest bid is below this value
    uint64 listingTime;
    uint64 duration;            // auction only
    uint8 listingType;
    uint8 state; 
}

struct Bid {
    address bidder;             // buyer and recipient
    address exchangeToken;      // payment token
    uint256 value;              // bid value
    bool active;                // is highest
}

enum AssetState { Idle, OnSale }
enum ListingType { Offchain, FixedPrice, Auction }
enum ComissionType { Universal }

struct AppStorage {
    mapping(address => bool) exchangeTokens;
    mapping(uint8 => uint24) comissions;
    mapping(bytes32 => bool) roots;
    uint256 refillNativeValue;

    // current assets: token id -> asset owner -> token address -> asset
    mapping(uint256 => mapping(address => mapping(address => Asset))) assets;
    // current bids: token id -> asset owner -> token address -> bid
    mapping(uint256 => mapping(address => mapping(address => Bid))) bids;

    mapping(address => uint256) minReservePriceTotal; // erc20 address -> value
    uint256 minAuctionDuration; 
    uint256 maxAuctionDuration; 
}

library LibAppStorage {

    function diamondStorage() internal pure returns (AppStorage storage ds) {
        assembly {
            ds.slot := 0
        }
    }
}

contract Modifiers {

    modifier onlyDiamondOwner() {
        LibDiamond.enforceIsContractOwner();
        _;
    }

    modifier isSender(address _address) {
        require(
            _address == LibCommon.msgSender(),
            "NOT_SENDER"
        );
        _;
    }

    modifier notEqual(address _address1, address _address2) {
        require(
            _address1 != _address2,
            "WRONG_ADDRESS"
        );
        _;
    }
}