const { expect } = require('chai')
const { ethers } = require('hardhat')
const path = require('path')
require('colors')
const axios = require('axios');

let bs_dir = path.resolve('../')
let contracts_dir = path.resolve('./')

let config = require(path.resolve(path.join(contracts_dir, 'config', 'deployed', 'config.json')))

const { MakeFunWeb3 } = require(path.join(bs_dir, 'blockchain_service/fun_web3.js'))

describe('Fune Web3'.bgBlack.brightGreen.bold, () => {

  let fun
  let new_account_1
  let new_account_2
  let new_account_3
  let host_account
  let nft_info
  let erc20_token

  before(async () => {
    // config.verbose = true

    // let result = await axios.post(
    //   // endpoint || this.config.subgraph_endpoint,
    //   "http://127.0.0.1:8000/subgraphs/name/fun-rise/funrise",
    //   {
    //     query:
    //     `
    //     {
    //       accounts (first: 100) {
    //         id
    //       }
    //     }
    //     `
    //   }
    // )
    // console.log(result.data.data)

    fun = await MakeFunWeb3(config)

    let accounts_path = path.join(__dirname, '../scripts/data', `accounts.tree.${fun.current.network.name}.json`)

    let accs = require(accounts_path)

    let adds = Object.keys(accs)

    let i = 0
    let address_1 = adds[i]
    let address_2 = adds[i + 1]
    let address_3 = adds[i + 2]

    new_account_1 = accs[address_1]
    new_account_1.address = address_1
    new_account_2 = accs[address_2]
    new_account_2.address = address_2
    new_account_3 = accs[address_3]
    new_account_3.address = address_3

    console.log(new_account_1, new_account_2, new_account_3)

    host_account = fun.current.host_account
    erc20_token = config.default_erc20_token
    wrapped_net_token = config.wrapped_net_token

    nft_info = {
      name: 'NFT',
      description: 'NFT Description',
      created_by: 'creator',
      image: {
        url: 'https://upload.wikimedia.org/wikipedia/commons/1/18/Ipfs-logo-1024-ice-text.png',
        mime_type: 'image/png',
        target_suffix: 'image.png',
        dimensions: '1024x1024'
      },
      media: [
        // {
        //   url: 'https://test-videos.co.uk/vids/bigbuckbunny/mp4/h264/360/Big_Buck_Bunny_360_10s_2MB.mp4',
        //   mime_type: 'video/mp4',
        //   target_suffix: 'video.mp4',
        //   dimensions: '640x360'
        // }
      ],
      tags: ['tag1', 'tag2'],
      external_url: 'https://www.google.com'
    }
  });

  describe('IPFS'.bgBlack.brightGreen.bold, () => {

    it('Should upload media to IPFS'.bgBlack.brightBlue.bold, async () => {

      const { url, gateway_url, gateway_image_url } = await fun.ipfs.UploadToIPFS(nft_info)

      console.log(`ipfs metadata url ${url}`.bgBlack.grey.bold)
      console.log(`ipfs metadata gateway url ${gateway_url}`.bgBlack.grey.bold)
      console.log(`ipfs image gateway url ${gateway_image_url}`.bgBlack.grey.bold)
    })
  })

  describe('ACCOUNTS'.bgBlack.brightGreen.bold, () => {

    xit('Should create accounts'.bgBlack.brightBlue.bold, async () => {

      // new_account_1

      let create = fun.CreateAccount({
        transfer_initial_net_coin: false,
        transfer_initial_default_erc20_token: true
      })

      while (true) {
        let result = await create.next()

        if (result.done) break

        new_account_1 = result.value.account

        console.log(`${result.value.part.reply_index} ${result.value.part.status}`.bgBlack.grey.bold)
      }

      expect(
        (await fun.GetNetCoinBalance({account_address: new_account_1.address})).balance
      ).to.eq(config.initial_user_net_coin_balance)

      expect(
        (await fun.GetERC20Balance({account_address: new_account_1.address, contract_address: erc20_token.address})).balance
      ).to.eq(config.initial_user_erc20_balance)

      // new_account_2

      create = fun.CreateAccount({
        transfer_initial_net_coin: false,
        transfer_initial_default_erc20_token: true
      })

      while (true) {
        let result = await create.next()

        if (result.done) break

        new_account_2 = result.value.account

        if (config.verbose) console.log(result.value)
      }
    })

    it('Should refill an account'.bgBlack.brightBlue.bold, async () => {

      let start_coin_balance = ethers.utils.parseEther(
        (await fun.GetNetCoinBalance({account_address: new_account_1.address})).balance
      )

      let refill = fun.RefillAccount({
        account: new_account_1
      })

      while (true) {
        let result = await refill.next()

        if (result.done) break

        var hash = result.value.part.transaction.hash

        console.log(`${result.value.part.reply_index} ${result.value.part.status}`.bgBlack.grey.bold)
      }

      let net_coin_balance = ethers.utils.parseEther(
        (await fun.GetNetCoinBalance({account_address: new_account_1.address})).balance
      )

      expect(net_coin_balance).to.be.above(start_coin_balance)

      let transaction = (await fun.GetTransaction({hash: hash})).transaction

      // minted & successful -> status == '1'
      // minted & faild -> status == '0'
      expect(
        transaction.status
      ).to.eq('1')

      // tx is pending -> transaction_index == -1
      expect(
        transaction.transaction_index
      ).to.be.above(-1)
    })

    it('Should swap ERC20 token for exact amount of network\'s native coin'.bgBlack.brightBlue.bold, async () => {

      let start_erc20_balance = ethers.utils.parseUnits(
        (await fun.GetERC20Balance({account_address: new_account_1.address, contract_address: erc20_token.address})).balance,
        erc20_token.decimals
      )
      if (config.verbose) console.log(`${new_account_1.address} ${erc20_token.symbol} balance: ${ethers.utils.formatUnits (start_erc20_balance, erc20_token.decimals)}`.bgBlack.white.bold)
      console.log(`${new_account_1.address} ${erc20_token.symbol} balance: ${ethers.utils.formatUnits (start_erc20_balance, erc20_token.decimals)}`.bgBlack.white.bold)

      let start_net_coin_balance = ethers.utils.parseEther(
        (await fun.GetNetCoinBalance({account_address: new_account_1.address})).balance
      )
      if (config.verbose) console.log(`${new_account_1.address} native coin balance: ${ethers.utils.formatEther(start_net_coin_balance)}`.bgBlack.white.bold)

      let net_coin_swap_amount = start_net_coin_balance.div(10)

      let estimated = await fun.EstimateSwapERC20ForNetCoin({
        contract_address: erc20_token.address,
        amount_out: ethers.utils.formatEther(net_coin_swap_amount)
      })

      console.log(`native coin out amount: ${ethers.utils.formatEther(net_coin_swap_amount)}`.bgBlack.white.bold)
      console.log(`estimated ERC20 input amount: ${estimated.amount_in}`.bgBlack.white.bold)

      let swap = fun.SwapERC20ForNetCoin({
        contract_address: erc20_token.address,
        amount_out: ethers.utils.formatEther(net_coin_swap_amount),
        account: new_account_1,
        slippage: '0.01' // 1%
      })

      while (true) {
        let result = await swap.next()

        if (result.done) break

        console.log(`${result.value.part.reply_index} ${result.value.part.status}`.bgBlack.grey.bold)
      }

      let end_erc20_balance = ethers.utils.parseUnits(
        (await fun.GetERC20Balance({account_address: new_account_1.address, contract_address: erc20_token.address})).balance,
        erc20_token.decimals
      )
      if (config.verbose) console.log(`${new_account_1.address} ${erc20_token.symbol} balance: ${ethers.utils.formatUnits (end_erc20_balance, erc20_token.decimals)}`.bgBlack.white.bold)

      let end_net_coin_balance = ethers.utils.parseEther(
        (await fun.GetNetCoinBalance({account_address: new_account_1.address})).balance
      )
      if (config.verbose) console.log(`${new_account_1.address} native coin balance: ${ethers.utils.formatEther(end_net_coin_balance)}`.bgBlack.white.bold)

      // expect(
      //   end_net_coin_balance
      // ).to.be.above(start_net_coin_balance)
    })

    it('Should swap ERC20 token for exact amount of network\'s native wrapped ERC20 token'.bgBlack.brightBlue.bold, async () => {

      let start_token_1_balance = ethers.utils.parseUnits(
        (await fun.GetERC20Balance({account_address: new_account_1.address, contract_address: erc20_token.address})).balance,
        erc20_token.decimals
      )
      if (config.verbose) console.log(`${new_account_1.address} ${erc20_token.symbol} balance: ${ethers.utils.formatUnits (start_token_1_balance, erc20_token.decimals)}`.bgBlack.white.bold)

      let start_token_2_balance = ethers.utils.parseUnits(
        (await fun.GetERC20Balance({account_address: new_account_1.address, contract_address: wrapped_net_token.address})).balance,
        wrapped_net_token.decimals
      )
      if (config.verbose) console.log(`${new_account_1.address} ${wrapped_net_token.symbol} balance: ${ethers.utils.formatUnits (start_token_2_balance, wrapped_net_token.decimals)}`.bgBlack.white.bold)

      let token_2_amount_out = start_token_1_balance.div(10)
      
      let estimated = await fun.EstimateSwapERC20ForERC20({
        contract_address_1: erc20_token.address,
        contract_address_2: wrapped_net_token.address,
        amount_out: ethers.utils.formatUnits(token_2_amount_out, wrapped_net_token.decimals)
      })

      if (config.verbose) console.log(`${wrapped_net_token.symbol} out amount: ${ethers.utils.formatUnits(token_2_amount_out, wrapped_net_token.decimals)}`.bgBlack.white.bold)
      if (config.verbose) console.log(`estimated ${erc20_token.symbol} input amount: ${estimated.amount_in}`.bgBlack.white.bold)

      let swap = fun.SwapERC20ForERC20({
        contract_address_1: erc20_token.address,
        contract_address_2: wrapped_net_token.address,
        amount_out: ethers.utils.formatUnits(token_2_amount_out, wrapped_net_token.decimals),
        account: new_account_1,
        slippage: '0.01' // 1%
      })

      while (true) {
        let result = await swap.next()

        if (result.done) break

        console.log(`${result.value.part.reply_index} ${result.value.part.status}`.bgBlack.grey.bold)
      }

      let mid_token_1_balance = ethers.utils.parseUnits(
        (await fun.GetERC20Balance({account_address: new_account_1.address, contract_address: erc20_token.address})).balance,
        erc20_token.decimals
      )
      if (config.verbose) console.log(`${new_account_1.address} ${erc20_token.symbol} balance: ${ethers.utils.formatUnits (mid_token_1_balance, erc20_token.decimals)}`.bgBlack.white.bold)

      let mid_token_2_balance = ethers.utils.parseUnits(
        (await fun.GetERC20Balance({account_address: new_account_1.address, contract_address: wrapped_net_token.address})).balance,
        wrapped_net_token.decimals
      )
      if (config.verbose) console.log(`${new_account_1.address} ${wrapped_net_token.symbol} balance: ${ethers.utils.formatUnits (mid_token_2_balance, wrapped_net_token.decimals)}`.bgBlack.white.bold)
    })

    it('Should transfer net coin from one account to another'.bgBlack.brightBlue.bold, async () => {

      const initial_balance = ethers.utils.parseEther(
        (await fun.GetNetCoinBalance({account_address: new_account_1.address})).balance
      )

      let transfer = fun.TransferNetCoin({
        account_from: new_account_2,
        address_to: new_account_1.address,
        value: ethers.utils.formatEther(initial_balance.div(100))
      })

      while (true) {
        let result = await transfer.next()

        if (result.done) break

        console.log(`${result.value.part.reply_index} ${result.value.part.status}`.bgBlack.grey.bold)
      }

      expect(
        ethers.utils.parseEther((await fun.GetNetCoinBalance({account_address: new_account_1.address})).balance)
      ).to.be.above(initial_balance)
    })

    describe('NFT'.bgBlack.brightGreen.bold, () => {

      let new_nft_supply = '5'
      let new_nft_price = '0.0001'
      let new_nft_reserve_price = '0.001'
      let new_nft_duration = (10 * 60).toString()

      it('Should create, mint and burn an NFT'.bgBlack.brightBlue.bold, async () => {

        let new_nft = null

        let create = fun.CreateNFT({
          creator: new_account_1,
          info: nft_info,
          royalty_info: {
            recipient_address: new_account_1.address,
            percentage: "2.5"
          },
          supply: new_nft_supply,
          need_mint: false
        })

        while (true) {
          let result = await create.next()

          if (result.done) break

          new_nft = result.value.nft

          console.log(`${result.value.part.reply_index} ${result.value.part.status}`.bgBlack.grey.bold)
        }

        let mint = fun.MintNFT({
          owner: new_account_1,
          nft: new_nft
        })

        while (true) {
          let result = await mint.next()

          if (result.done) break

          new_nft = result.value.nft

          console.log(`${result.value.part.reply_index} ${result.value.part.status}`.bgBlack.grey.bold)
        }

        expect(
          (await fun.GetNFTBalance({
            account_address: new_account_1.address,
            contract_address: new_nft.contract_address,
            token_id: new_nft.token_id
          })).balance
        ).to.eq(new_nft_supply)

        let burn = fun.BurnNFT({
          owner: new_account_1,
          token_id: new_nft.token_id,
          amount: new_nft_supply
        })

        while (true) {
          let result = await burn.next()

          if (result.done) break

          console.log(`${result.value.part.reply_index} ${result.value.part.status}`.bgBlack.grey.bold)
        }

        expect(
          (await fun.GetNFTBalance({
            account_address: new_account_1.address,
            contract_address: new_nft.contract_address,
            token_id: new_nft.token_id
          })).balance
        ).to.eq('0')
      })

      it('Should create, mint and transfer an NFT'.bgBlack.brightBlue.bold, async () => {

        let new_nft = null

        let create = fun.CreateNFT({
          creator: new_account_1,
          info: nft_info,
          royalty_info: {
            recipient_address: new_account_1.address,
            percentage: "2.5"
          },
          supply: new_nft_supply,
          need_mint: true
        })

        while (true) {
          let result = await create.next()

          if (result.done) break

          new_nft = result.value.nft

          console.log(`${result.value.part.reply_index} ${result.value.part.status}`.bgBlack.grey.bold)
        }

        let balance = (await fun.GetNFTBalance({
          account_address: new_account_2.address,
          contract_address: new_nft.contract_address,
          token_id: new_nft.token_id
        })).balance

        let amount = new_nft_supply

        let transfer = fun.TransferNFT({
          account_from: new_account_1,
          contract_address: new_nft.contract_address,
          address_to: new_account_2.address,
          amount: amount,
          token_id: new_nft.token_id
        })
          
        while (true) {
          let result = await transfer.next()

          if (result.done) break

          console.log(`${result.value.part.reply_index} ${result.value.part.status}`.bgBlack.grey.bold)
        }

        let updated_balance = (await fun.GetNFTBalance({
          account_address: new_account_2.address,
          contract_address: new_nft.contract_address,
          token_id: new_nft.token_id
        })).balance

        expect(
          parseInt(updated_balance)
        ).to.eq(parseInt(balance) + parseInt(amount))
      })

      it('Should mint an NFT created by another account'.bgBlack.brightBlue.bold, async () => {

        let new_nft = null

        let create = fun.CreateNFT({
          creator: new_account_1,
          info: nft_info,
          royalty_info: {
            recipient_address: new_account_1.address,
            percentage: "2.5"
          },
          supply: new_nft_supply,
          need_mint: false
        })

        while (true) {
          let result = await create.next()

          if (result.done) break

          new_nft = result.value.nft

          console.log(`${result.value.part.reply_index} ${result.value.part.status}`.bgBlack.grey.bold)
        }

        let mint = fun.MintNFT({
          minter: new_account_2,
          owner: new_account_1,
          nft: new_nft,
        })

        while (true) {
          let result = await mint.next()

          if (result.done) break

          new_nft = result.value.nft

          console.log(`${result.value.part.reply_index} ${result.value.part.status}`.bgBlack.grey.bold)
        }

        expect(
          (await fun.GetNFTBalance({
            account_address: new_account_1.address,
            contract_address: new_nft.contract_address,
            token_id: new_nft.token_id
          })).balance
        ).to.eq(new_nft_supply)
      })

      it('Should redeem an NFT minted by another account'.bgBlack.brightBlue.bold, async () => {

        let new_nft = null

        let create = fun.CreateNFT({
          creator: new_account_1,
          info: nft_info,
          royalty_info: {
            recipient_address: new_account_1.address,
            percentage: "2.5"
          },
          supply: new_nft_supply,
          need_mint: false
        })

        while (true) {
          let result = await create.next()

          if (result.done) break

          new_nft = result.value.nft

          console.log(`${result.value.part.reply_index} ${result.value.part.status}`.bgBlack.grey.bold)
        }

        let mint = fun.MintNFT({
          owner: new_account_1,
          nft: new_nft
        })

        while (true) {
          let result = await mint.next()

          if (result.done) break

          new_nft = result.value.nft

          console.log(`${result.value.part.reply_index} ${result.value.part.status}`.bgBlack.grey.bold)
        }

        let balance = (await fun.GetNFTBalance({
          account_address: new_account_2.address,
          contract_address: new_nft.contract_address,
          token_id: new_nft.token_id
        })).balance

        let redeem_amount  = '3'

        let redeem = fun.RedeemNFT({
          payer: new_account_2,
          owner: new_account_1,
          address_to: new_account_2.address,
          nft: new_nft,
          asset: {
            token_id: new_nft.token_id,
            contract_address: new_nft.contract_address,
            owner_address: new_account_1.address,
            exchange_token: erc20_token.address,
            amount: new_nft_supply,
            fixed_price: new_nft_price,
            listing_type: 0
          },
          amount: redeem_amount
        })

        while (true) {
          let result = await redeem.next()

          if (result.done) break

          console.log(`${result.value.part.reply_index} ${result.value.part.status}`.bgBlack.grey.bold)
        }

        let updated_balance = (await fun.GetNFTBalance({
          account_address: new_account_2.address,
          contract_address: new_nft.contract_address,
          token_id: new_nft.token_id
        })).balance

        expect(
          parseInt(updated_balance)
        ).to.eq(parseInt(balance) + parseInt(redeem_amount))
      });

      it('Should mint and redeem an NFT created by another account'.bgBlack.brightBlue.bold, async () => {

        let new_nft = null

        let create = fun.CreateNFT({
          creator: new_account_1,
          info: nft_info,
          royalty_info: {
            recipient_address: new_account_1.address,
            percentage: "2.5"
          },
          supply: new_nft_supply,
          need_mint: false
        })

        while (true) {
          let result = await create.next()

          if (result.done) break

          new_nft = result.value.nft

          console.log(`${result.value.part.reply_index} ${result.value.part.status}`.bgBlack.grey.bold)
        }

        let redeem_amount  = '3'
        
        let redeem = fun.RedeemNFT({
          payer: new_account_2,
          owner: new_account_1,
          address_to: new_account_2.address,
          nft: new_nft,
          asset: {
            token_id: new_nft.token_id,
            contract_address: new_nft.contract_address,
            owner_address: new_account_1.address,
            exchange_token: erc20_token.address,
            amount: new_nft_supply,
            fixed_price: new_nft_price,
            listing_type: 0
          },
          amount: redeem_amount
        })

        while (true) {
          let result = await redeem.next()

          if (result.done) break

          console.log(`${result.value.part.reply_index} ${result.value.part.status}`.bgBlack.grey.bold)
        }

        let updated_balance = (await fun.GetNFTBalance({
          account_address: new_account_2.address,
          contract_address: new_nft.contract_address,
          token_id: new_nft.token_id
        })).balance

        expect(
          parseInt(updated_balance)
        ).to.eq(parseInt(redeem_amount))
      });

      describe('MARKETPLACE'.bgBlack.brightGreen.bold, () => {

        let new_nft = null

        before(async () => {
      
          let create = fun.CreateNFT({
            creator: new_account_2,
            info: nft_info,
            royalty_info: {
              recipient_address: new_account_1.address,
              percentage: "2.5"
            },
            supply: new_nft_supply,
            need_mint: true
          })
  
          while (true) {
            let result = await create.next()
  
            if (result.done) break
  
            new_nft = result.value.nft
  
            console.log(`${result.value.part.reply_index} ${result.value.part.status}`.bgBlack.grey.bold)
          }
        });

        it('FixedPrice: Should list and unlist an NFT'.bgBlack.brightBlue.bold, async () => {

          let list = fun.ListNFT({
            owner: new_account_2,
            asset: {
              token_id: new_nft.token_id,
              contract_address: new_nft.contract_address,
              owner_address: new_account_2.address,
              exchange_token: erc20_token.address,
              amount: new_nft_supply,
              fixed_price: new_nft_price,
              listing_type: fun.ListingType.FixedPrice,
            }
          })
            
          while (true) {
            let result = await list.next()

            if (result.done) break

            console.log(`${result.value.part.reply_index} ${result.value.part.status}`.bgBlack.grey.bold)
          }

          let asset = (await fun.GetNFTAsset({
            owner_address: new_account_2.address,
            contract_address: new_nft.contract_address,
            token_id: ethers.BigNumber.from(new_nft.token_id)
          })).asset

          expect(asset.listed).to.eq(true)

          let unlist = fun.UnlistNFT({
            owner: new_account_2,
            contract_address: new_nft.contract_address,
            token_id: new_nft.token_id,
          })

          while (true) {
            let result = await unlist.next()

            if (result.done) break

            console.log(`${result.value.part.reply_index} ${result.value.part.status}`.bgBlack.grey.bold)
          }

          let updated_asset = (await fun.GetNFTAsset({
            owner_address: new_account_2.address,
            contract_address: new_nft.contract_address,
            token_id: ethers.BigNumber.from(new_nft.token_id)
          })).asset

          expect(updated_asset.listed).to.eq(false)
        })

        it('FixedPrice: Should list and redeem an NFT'.bgBlack.brightBlue.bold, async () => {

          let list = fun.ListNFT({
            owner: new_account_2,
            asset: {
              token_id: new_nft.token_id,
              contract_address: new_nft.contract_address,
              owner_address: new_account_2.address,
              exchange_token: erc20_token.address,
              amount: new_nft_supply,
              fixed_price: new_nft_price,
              listing_type: fun.ListingType.FixedPrice,
            }
          })
            
          while (true) {
            let result = await list.next()

            if (result.done) break

            console.log(`${result.value.part.reply_index} ${result.value.part.status}`.bgBlack.grey.bold)
          }

          let asset = (await fun.GetNFTAsset({
            owner_address: new_account_2.address,
            contract_address: new_nft.contract_address,
            token_id: new_nft.token_id
          })).asset

          expect(asset.listed).to.eq(true)

          let redeem_amount = asset.amount

          let balance = (await fun.GetNFTBalance({
            account_address: new_account_1.address,
            contract_address: new_nft.contract_address,
            token_id: new_nft.token_id
          })).balance

          let redeem = fun.RedeemNFT({
            payer: new_account_1,
            owner: new_account_2,
            asset: asset,
            address_to: new_account_1.address,
            amount: redeem_amount
          })
            
          while (true) {
            let result = await redeem.next()

            if (result.done) break

            console.log(`${result.value.part.reply_index} ${result.value.part.status}`.bgBlack.grey.bold)
          }

          let updated_balance = (await fun.GetNFTBalance({
            account_address: new_account_1.address,
            contract_address: new_nft.contract_address,
            token_id: new_nft.token_id
          })).balance
  
          expect(
            parseInt(updated_balance)
          ).to.eq(parseInt(balance) + parseInt(redeem_amount))

          let updated_asset = (await fun.GetNFTAsset({
            owner_address: new_account_2.address,
            contract_address: new_nft.contract_address,
            token_id: new_nft.token_id
          })).asset

          expect(
            parseInt(updated_asset.amount)
          ).to.eq(parseInt(asset.amount) - parseInt(redeem_amount))
        })

        it('Auction: Should create and cancel'.bgBlack.brightBlue.bold, async () => {

          let list = fun.ListNFT({
            owner: new_account_1,
            asset: {
              token_id: new_nft.token_id,
              contract_address: new_nft.contract_address,
              owner_address: new_account_1.address,
              exchange_token: erc20_token.address,
              amount: new_nft_supply,
              starting_price: new_nft_price,
              reserve_price: new_nft_reserve_price,
              duration: new_nft_duration,
              listing_type: fun.ListingType.Auction,
            }
          })
            
          while (true) {
            let result = await list.next()

            if (result.done) break

            console.log(`${result.value.part.reply_index} ${result.value.part.status}`.bgBlack.grey.bold)
          }

          let asset = (await fun.GetNFTAsset({
            owner_address: new_account_1.address,
            contract_address: new_nft.contract_address,
            token_id: ethers.BigNumber.from(new_nft.token_id),
          })).asset

          expect(asset.listed).to.eq(true)
          expect(
            (await fun.GetNFTBalance({
              account_address: fun.contracts['Diamond'].address,
              contract_address: new_nft.contract_address,
              token_id: new_nft.token_id
            })).balance
          ).to.eq(new_nft_supply)

          let cancel = fun.ResolveAuction({
            owner: new_account_1,
            contract_address: new_nft.contract_address,
            token_id: new_nft.token_id,
            accept_highest_bid: false
          })

          while (true) {
            let result = await cancel.next()

            if (result.done) break

            console.log(`${result.value.part.reply_index} ${result.value.part.status}`.bgBlack.grey.bold)
          }

          asset = (await fun.GetNFTAsset({
            owner_address: new_account_1.address,
            contract_address: new_nft.contract_address,
            token_id: ethers.BigNumber.from(new_nft.token_id),
          })).asset

          expect(asset.listed).to.eq(false)
          expect(
            (await fun.GetNFTBalance({
              account_address: fun.contracts['Diamond'].address,
              contract_address: new_nft.contract_address,
              token_id: new_nft.token_id
            })).balance
          ).to.eq('0')
        })

        it('Auction: Should create, bid and cancel'.bgBlack.brightBlue.bold, async () => {

          let list = fun.ListNFT({
            owner: new_account_1,
            asset: {
              token_id: new_nft.token_id,
              contract_address: new_nft.contract_address,
              owner_address: new_account_1.address,
              exchange_token: erc20_token.address,
              amount: new_nft_supply,
              starting_price: new_nft_price,
              reserve_price: new_nft_reserve_price,
              duration: new_nft_duration,
              listing_type: fun.ListingType.Auction,
            }
          })
            
          while (true) {
            let result = await list.next()

            if (result.done) break

            console.log(`${result.value.part.reply_index} ${result.value.part.status}`.bgBlack.grey.bold)
          }

          let asset = (await fun.GetNFTAsset({
            owner_address: new_account_1.address,
            contract_address: new_nft.contract_address,
            token_id: ethers.BigNumber.from(new_nft.token_id),
          })).asset

          expect(asset.listed).to.eq(true)
          expect(
            (await fun.GetNFTBalance({
              account_address: fun.contracts['Diamond'].address,
              contract_address: new_nft.contract_address,
              token_id: new_nft.token_id
            })).balance
          ).to.eq(new_nft_supply)

          let bid = fun.MakeBid({
            bidder: new_account_2,
            asset: asset,
            value: '0.001',
          })

          while (true) {
            let result = await bid.next()

            if (result.done) break

            console.log(`${result.value.part.reply_index} ${result.value.part.status}`.bgBlack.grey.bold)
          }

          let bid2 = fun.MakeBid({
            bidder: new_account_3,
            asset: asset,
            value: '0.002',
          })

          while (true) {
            let result = await bid2.next()

            if (result.done) break

            console.log(`${result.value.part.reply_index} ${result.value.part.status}`.bgBlack.grey.bold)
          }

          let resolve = fun.ResolveAuction({
            owner: new_account_1,
            contract_address: new_nft.contract_address,
            token_id: new_nft.token_id,
            accept_highest_bid: true
          })

          while (true) {
            let result = await resolve.next()

            if (result.done) break

            console.log(`${result.value.part.reply_index} ${result.value.part.status}`.bgBlack.grey.bold)
          }

          asset = (await fun.GetNFTAsset({
            owner_address: new_account_1.address,
            contract_address: new_nft.contract_address,
            token_id: ethers.BigNumber.from(new_nft.token_id),
          })).asset

          expect(asset.listed).to.eq(false)
          expect(
            (await fun.GetNFTBalance({
              account_address: fun.contracts['Diamond'].address,
              contract_address: new_nft.contract_address,
              token_id: new_nft.token_id
            })).balance
          ).to.eq('0')
          expect(
            (await fun.GetNFTBalance({
              account_address: new_account_3.address,
              contract_address: new_nft.contract_address,
              token_id: new_nft.token_id
            })).balance
          ).to.eq(new_nft_supply)
        })
      })
    })
  })
})